
import findConsistencyIndexesPCM as CONS_INDEX
import checkConsistencyPCM as CONS
import findLocalWs as LOC_W
import copy


def findSigma(a, b): # a, b - матрицы (список списков)
    
    sigma = None
    if len(a) == len(b):
        s = 0
        for i in range(len(a)):
            for j in range(len(a)):
                s += pow(a[i][j]-b[i][j], 2)

        sigma = pow(s,1/2) / len(a)
 #       print(s)
        
    else:
        print("Ошибка: размерности матриц не совпадают")        
    
    return sigma


def findDelta(a, b): # a, b - матрицы (список списков)
    c = list()
    delta = None
    
    if len(a) == len(b):
        for i in range(len(a)):
            c.append([])
            for j in range(len(a)):
                c[i].append(abs(a[i][j]-b[i][j]))

        delta = max(max(c)) # максимальный эл-т матрицы с
#        print(c)
        
    else:
        print("Ошибка: размерности матриц не совпадают")        
    
    return delta



def findMultCorrection(PCM,alfa):

    N = len(PCM)
    PCM_cor = list() #матрица NxN 
    sigma = None
    delta = None
    
    k = 0
    
    while not CONS.isConsistCR(PCM):

        PCM_cor = list() 
        v = LOC_W.EigenvectorMethod(PCM)

        for i in range(N):
            PCM_cor.append([])
            for j in range(N):
                PCM_cor[i].append( pow(PCM[i][j],alfa) * pow(v[i]/v[j], 1-alfa))
        
        sigma = findSigma(PCM_cor, PCM)
        delta = findDelta(PCM_cor, PCM)
        
        PCM = copy.deepcopy(PCM_cor)

        k +=1

##        print("-------------------------")
##        print("Corrected PCM")
##        print(PCM_cor)
##        print("CR(PCM_cor)= "+ str(CONS_INDEX.ConsistencyRatio(PCM_cor)))
##        print("sigma and delta coefs")
##        print([sigma, delta])
       

    return {"PCM_cor":PCM_cor, "numIterat":k, "coefs":[sigma, delta]}



def findAdditivCorrection(PCM,alfa):

    N = len(PCM)
    PCM_cor = copy.deepcopy(PCM)
    sigma = None
    delta = None

    k = 0
    while not CONS.isConsistCR(PCM):
        
        v = LOC_W.EigenvectorMethod(PCM)

        for i in range(N):
            for j in range(N):
                if j>=i:
                    PCM_cor[i][j] = PCM[i][j]*alfa + (v[i]/v[j])*(1-alfa)
        for i in range(N):
            for j in range(N):
                if j<i:
                    PCM_cor[i][j] = 1 / PCM_cor[j][i]
        
        sigma = findSigma(PCM_cor, PCM)
        delta = findDelta(PCM_cor, PCM)
        
        PCM = copy.deepcopy(PCM_cor)

        k +=1

##        print("-------------------------")
##        print("Corrected PCM")
##        print(PCM_cor)
##        print("CR(PCM_cor)= "+ str(CONS_INDEX.ConsistencyRatio(PCM_cor)))
##        print("sigma and delta coefs")
##        print([sigma, delta])
       

    return {"PCM_cor":PCM_cor, "numIterat":k, "coefs":[sigma, delta]}
