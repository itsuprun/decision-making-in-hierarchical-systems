import math
from numpy import linalg as LA


def ConsistencyRatio(PCM):
    index = None    

    N = len(PCM)

    if N>1:
        MRCI={3:0.52, 4:0.89, 5:1.11, 6:1.25, 7:1.35, 8:1.40, 9:1.45, 10:1.49, 11:1.52, 12:1.54, 13:1.56, 14:1.58, 15:1.59}

        lamda, eigvect = LA.eig(PCM)
#        print(lamda)

        lamdaMax = (max(lamda)).real
#        print(lamdaMax)

        CI = (lamdaMax - N)/(N-1)

        if (N>2):
            CR = CI / MRCI[N]
        else:
            CR = 0
#        print("CI="+str(CI)+" CR="+str(CR))


        index = CR
    return index


def GeometricConsistencyIndex(PCM):
    index = None
    N = len(PCM)

    if N>1:            
        v=list()
        E=list()
        for i in range(N):
            v.append(1/N)
        for i in range(N):
            E.append([])
            for j in range(N):
                E[i].append(1)

        for i in range(N):
            prod=1
            for j in range(N):
                prod = prod*PCM[i][j]
            v[i] = pow(prod,1/N)

        for i in range(N):
            for j in range(N):
                E[i][j] = PCM[i][j]*v[j]/v[i]


        s=0
        for i in range(N):
            for j in range(N):
                if i<j:
                    s += pow(math.log(E[i][j]),2)

        if (N>2):
            GCI = s / ((N-1)*(N-2)/2)
        else:
            GCI = 0
#        print("GCI="+str(GCI))


        index = GCI           
    return index


def HarmonicConsistencyRatio(PCM):
    index = None
    
    N = len(PCM)
        
    if N>1:
        HRCI={3:0.52, 4:0.89, 5:1.11, 6:1.25, 7:1.35, 8:1.40, 9:1.45, 10:1.49, 11:1.52, 12:1.54, 13:1.56, 14:1.58, 15:1.59}

        s=list() 
        for j in range(N):
            s.append(0)
            for i in range(N):
                s[j] = s[j] + PCM[i][j]
            s[j] = pow(s[j],-1)
            
        HM = N / sum(s)
        HCI = (HM - N)*(N+1) / (N*(N-1))

        if (N>2):
            HCR = HCI / HRCI[N]
        else:
            HCR = 0

#        print("HCI="+str(HCI)+" HCR="+str(HCR))
        
        index = HCR
    return index


def ConsistencyRatio_threshold(N):
    if N==3:
        threshold=0.05
    elif N==4:
        threshold=0.08
    elif N>=5:
        threshold=0.1

    return threshold


def GeometricConsistencyIndex_threshold(N):
    if N==3:
        threshold=0.157
    elif N==4:
        threshold=0.353
    elif N>=5:
        threshold=0.370

    return threshold


def HarmonicConsistencyRatio_threshold(N):

    return ConsistencyRatio_threshold(N)
