from numpy import linalg as LA
import findConsistencyIndexesPCM as CONS_INDEX
import copy
import math

def findUFO_methodCI(PCM): #метод CI для укороченных МПС

    CI = list()
    index1=0
    index2=0

    if CONS_INDEX.ConsistencyRatio(PCM) > 0.000001:
        for k in range(len(PCM)):
            PCM_reduced = copy.deepcopy(PCM)
            PCM_reduced[k:(k+1)] = []  #удаляем k-ю строку в МПС

            for j in range(len(PCM_reduced)):
                PCM_reduced[j][k:(k+1)] = [] #в каждой строке укороченной МПС удаляем k-й эл-т
                                             # = удаляем k-й столбец в исходной МПС  
            
    ##        print(PCM_reduced)
    ##        print("------------------------------")
            currCI = CONS_INDEX.ConsistencyRatio(PCM_reduced)
            print(f"При виключенні альтернативи {k} CI = {currCI}")
            CI.append(currCI)
            
    #    print(CI)
        
        index1 = CI.index(min(CI)) #возвращвет индекс эл-та, имеющего минимал.значение   

    #    del CI[index1] #исключаем из списка CI эл-т с индексом index1 (имеющий минимальное значение)
        CI[index1] = 1000                               

        index2 = CI.index(min(CI)) #возвращвет индекс эл-та, имеющего минимал.значение  

#    print("Наиболее несогласован элемент МПС с индексами "+ str(index1) + " и " + str(index2))

    return [index1,index2]




def findUFO_methodTransitivities(PCM): #метод транзитивностей
    N = len(PCM)

    indexes = list() # вектор, каждый эл-т - список из двух индексов [i,j], соответствует элементу векторa Tr
    
    if CONS_INDEX.ConsistencyRatio(PCM) > 0.000001:
        Tr = list() # вектор
        u = 0    
        for i in range(N):
            for j in range(N):
                if i<j:
                    s = 0
                    for k in range(N):
                        if (k!=j) and (k!=i):
                            det = PCM[i][j]*PCM[j][k]/PCM[i][k] + PCM[i][k]/(PCM[i][j]*PCM[j][k]) - 2
                            s += det
                    Tr.append(s)
                    indexes.append([i,j])
                    u +=1
                    
        index1 = Tr.index(max(Tr)) #возвращвет индекс эл-та, имеющего макс.значение
    else:
        indexes.append(0)
        index1=0

    return indexes[index1]    


def sumElemsList(x):
    s = 0
    N = len(x)
    for i in range(N):
        s += x[i]
        
    return s


def findUFO_methodXiSquares(PCM): #метод Xi-квадрат
    N = len(PCM)

    indexes = list() # вектор пар индексов (списков [i,j])

    if CONS_INDEX.ConsistencyRatio(PCM) > 0.000001:
        T = list() # матрица NxN
        Xi = list() # матрица NxN
        sum1 = 0

        for k in range(N):
            sum1 += sumElemsList(PCM[k])

        for i in range(N):
            T.append([])
            sum2 = sumElemsList(PCM[i])
            for j in range(N):
                sum3 = 0
                for l in range(N):
                    sum3 += PCM[l][j]
                T[i].append((sum2*sum3)/sum1)

    #    print("Матрица T")
    #    print(T)

        for i in range(N):
            Xi.append([])
            for j in range(N):
                Xi[i].append(pow((PCM[i][j] - T[i][j]),2) / T[i][j])

    #    print("Матрица Xi")
    #    print(Xi)
        
        sum1 = 0
        for k in range(N):
            sum1 += sumElemsList(Xi[k])

        meanXi = sum1 / pow(N,2)

        d = 0
        for i in range(N):
            for j in range(N):
                d += pow((Xi[i][j] - meanXi),2)

        stdDevXi = pow(d / (pow(N,2) - 1) , 1/2)

        left = meanXi - stdDevXi
        right= meanXi + stdDevXi


        for i in range(N):
            for j in range(N):
                if (Xi[i][j] < left) or (Xi[i][j] > right):
                    indexes.append([i,j])


#    print("Множество наиболее несогласованных элементов МПС характеризуется индексами")
#    print(indexes)

    return indexes



def findUFO_methodOutflow(PCM): #метод потоков OutFlow
    N = len(PCM)
    indexes = list() # вектор пар индексов (списков [i,j])


    if CONS_INDEX.ConsistencyRatio(PCM) > 0.000001:
        fi = list() # вектор N
        diff = list() # матрица NxN
        gamma = list() # вектор

        for i in range(N):
            fi.append(0)
            for j in range(N):
                if PCM[i][j] > 1:
                    fi[i] +=1

    #    print("Вектор Fi")
    #    print(fi)

        for i in range(N):
            diff.append([])
            for j in range(N):
                if PCM[i][j] > 1:
                    diff[i].append(fi[j] - fi[i])
                else:
                    diff[i].append(-100)

    #    print("Матрица diff")
    #    print(diff)
        
    ##    row = max(diff) # возвращает строку (список), содержащий макс.значение матрицы
    ##    index1 = diff.index(row) # номер строки матрицы diff, содержащей макс.значение
    ##    index2 = row.index(max(row)) # номер максимального эл-та в списке row
    ##                                 # = номер столбца матр. diff, содержащий макс.значение

        for i in range(N):
            for j in range(N):
                if (diff[i][j] == max(max(diff))):
                    indexes.append([i,j])

    #    print("Матрица Indexes индексов максимальных эл-тов матрицы diff")
    #    print(indexes)
        
        if len(indexes) > 1:
            for x in range(len(indexes)):
                i = indexes[x][0]
                j = indexes[x][1]
                s = 0

                for k in range(N):
                    if (k != i) and (k != j):
                        s += math.log(PCM[i][j]) - math.log(PCM[i][k] * PCM[k][j])

                gamma.append(s/(N-2))
        else:
            gamma.append(0)

    #    print("Вектор gamma")
    #    print(gamma)

        res = gamma.index(max(gamma))

    ##    index1 = indexes[res][0]
    ##    index2 = indexes[res][1]
    else:
        indexes.append(0)
        res=0

##    print("Наиболее несогласован элемент МПС с индексами "+ str(index1) + " и " + str(index2))
#    print("Наиболее несогласован элемент МПС с индексами ")
#    print(indexes[res])

    return indexes[res]




def findUFO_methodModifiedOutflow(PCM): #модифицированный метод потоков MOutFlow
    N = len(PCM)

    indexes = list() # вектор пар индексов (списков [i,j])


    if CONS_INDEX.ConsistencyRatio(PCM) > 0.000001:
        fi_plus = list() # вектор N
        fi_minus = list() # вектор N
        diff_minus = list() # матрица NxN
        diff_plus = list() # матрица NxN
        diff = list() # матрица NxN
        gamma = list() # вектор

        for i in range(N):
            fi_minus.append(0)
            fi_plus.append(0)
            for j in range(N):
                if PCM[i][j] > 1:
                    fi_minus[i] +=1
                if PCM[i][j] < 1:
                    fi_plus[i] +=1
                    
    ##    print("Вектор Fi_minus")
    ##    print(fi_minus)
    ##    print("Вектор Fi_plus")
    ##    print(fi_plus)

        for i in range(N):
            diff_minus.append([])
            diff_plus.append([])
            for j in range(N):
                if PCM[i][j] > 1:
                    diff_minus[i].append(fi_minus[j] - fi_minus[i])
                    diff_plus[i].append(fi_plus[i] - fi_plus[j])
                else:
                    diff_minus[i].append(-100)
                    diff_plus[i].append(-100)

        for i in range(N):
            diff.append([])
            for j in range(N):
                diff[i].append( max(diff_minus[i][j], diff_plus[i][j]) )
                    
    ##    print("Матрица diff")
    ##    print(diff)
        
    ##    row = max(diff) # возвращает строку (список), содержащий макс.значение матрицы
    ##    index1 = diff.index(row) # номер строки матрицы diff, содержащей макс.значение
    ##    index2 = row.index(max(row)) # номер максимального эл-та в списке row
    ##                                 # = номер столбца матр. diff, содержащий макс.значение

        for i in range(N):
            for j in range(N):
                if (diff[i][j] == max(max(diff))):
                    indexes.append([i,j])

    ##    print("Матрица Indexes индексов максимальных эл-тов матрицы diff")
    ##    print(indexes)
        
        if len(indexes) > 1:
            for x in range(len(indexes)):
                i = indexes[x][0]
                j = indexes[x][1]
                s = 0

                for k in range(N):
                    if (k != i) and (k != j):
                        s += math.log(PCM[i][j]) - math.log(PCM[i][k] * PCM[k][j])

                gamma.append(s/(N-2))
        else:
            gamma.append(0)

    ##    print("Вектор gamma")
    ##    print(gamma)

        res = gamma.index(max(gamma))

    ##    index1 = indexes[res][0]
    ##    index2 = indexes[res][1]

    else:
        indexes.append(0)
        res=0

        
##    print("Наиболее несогласован элемент МПС с индексами "+ str(index1) + " и " + str(index2))
##    print("Наиболее несогласован элемент МПС с индексами ")
##    print(indexes[res])


    return indexes[res]
