

def isInverseSymmetric(PCM):
    flag = True
    N = len(PCM)

    if N>1:
        for i in range(N):
            for j in range(N):
                if i<j:
                    if PCM[j][i] != 1 / PCM[i][j]:
                        flag = False
                        break

    return flag


def isInverseSymmetricIPCM(L,U):
    flag = True
    N1 = len(L)
    N2 = len(U)

    if N1 == N2:
        if N1 > 1:
            for i in range(N1):
                for j in range(N1):
                    if i<j:
                        if L[j][i] != 1 / U[i][j]:
                            flag = False
                            break
                        if U[j][i] != 1 / L[i][j]:    
                            flag = False
                            break
    else:
        flag = False  #printf("ошибка: размерности не совпадают")
        
    return flag



def isInFundamentalScale(PCM):

    Scale = (1, 2, 3, 4, 5, 6, 7, 8, 9, 1/2, 1/3, 1/4, 1/5, 1/6, 1/7, 1/8, 1/9)
#    for i in range(1, len(Scale)):
#        Scale.append(1 / Scale[i])

#    print(str(Scale))

    flag = True
    N = len(PCM)
    for i in range(N):
        for j in range(N):
            if not(PCM[i][j] in Scale):
                flag = False
                break


    return flag
