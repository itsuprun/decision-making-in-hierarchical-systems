import interfaceKeyboard as INPUT

import structureHierachy as STRUCT_HIER
import checkCorrectness as CORRECT
import findConsistencyIndexesPCM as CONS_INDEX
import checkConsistencyPCM as CONS
import findUFOs as UFO
import findImprovedPCM as IMPROV_PCM
import findLocalWs as LOC_W
import findGlobalWs as GLOB_W
import calculationHierarchy as CALC



# Test No 1
##Hier = {"E0_0": {'GlobW': 1, 'LocalW': [0.66666666666666663, 0.33333333333333331], 'About': 'This is a main goal', 'IdLevel': 0, 'Parents': [], 'Name': 'Root', 'Childs': ['E1_0', 'E1_1'], 'PCM': [[1, 2.0], [0.5, 1]]},
##"E1_0": {'GlobW': 1, 'LocalW': [0.75, 0.25], 'About': 'About E1_0', 'IdLevel': 1, 'Parents': ['E0_0'], 'Name': 'Child No 0 of the E0_0', 'Childs': ['E2_0', 'E2_1'], 'PCM': [[1, 3.0], [0.3333333333333333, 1]]},
##"E1_1": {'GlobW': 1, 'LocalW': [0.5, 0.24999999999999994, 0.25], 'About': 'About E1_1', 'IdLevel': 1, 'Parents': ['E0_0'], 'Name': 'Child No 1 of the E0_0', 'Childs': ['E3_0', 'E3_1', 'E3_2'], 'PCM': [[1, 2.0, 2.0], [0.5, 1, 1.0], [0.5, 1.0, 1]]},
##"E2_0": {'GlobW': 1, 'LocalW': [0.1999999999906667, 0.60000000001199993, 0.19999999999733337], 'About': 'About E2_0', 'IdLevel': 2, 'Parents': ['E1_0'], 'Name': 'Child No 0 of the E1_0', 'Childs': ['E3_0', 'E3_1', 'E3_2'], 'PCM': [[1, 0.3333333333, 1.0], [3.0000000003, 1, 3.0], [1.0, 0.3333333333333333, 1]]},
##"E2_1": {'GlobW': 1, 'LocalW': [0.14285714285714288, 0.28571428571428564, 0.57142857142857151], 'About': 'About E2_1', 'IdLevel': 2, 'Parents': ['E1_0'], 'Name': 'Child No 1 of the E1_0', 'Childs': ['E3_0', 'E3_1', 'E3_2'], 'PCM': [[1, 0.5, 0.25], [2.0, 1, 0.5], [4.0, 2.0, 1]]},
##"E3_0": {'GlobW': 1, 'LocalW': [], 'About': 'About E3_0', 'IdLevel': 3, 'Parents': ['E1_1', 'E2_0', 'E2_1'], 'Name': 'Child No 0 of the E2_0', 'Childs': [], 'PCM': []},
##"E3_1": {'GlobW': 1, 'LocalW': [], 'About': 'About E3_1', 'IdLevel': 3, 'Parents': ['E1_1', 'E2_0', 'E2_1'], 'Name': 'Child No 1 of the E2_0', 'Childs': [], 'PCM': []},
##"E3_2": {'GlobW': 1, 'LocalW': [], 'About': 'About E3_2', 'IdLevel': 3, 'Parents': ['E1_1', 'E2_0', 'E2_1'], 'Name': 'Child No 2 of the E2_0', 'Childs': [], 'PCM': []}}

# Test No 2
##Hier = {"E0_0": {'Name': 'Root', 'Parents': [], 'About': 'This is a main goal', 'Childs': ['E1_0', 'E1_1'], 'PCM': [[1, 2.0], [0.5, 1]], 'IdLevel': 0, 'LocalW': [0.66666666666666663, 0.33333333333333331], 'GlobW': 1},
##"E1_0": {'Name': 'Child No 0 of the E0_0', 'Parents': ['E0_0'], 'About': 'About E1_0', 'Childs': ['E2_0', 'E2_1'], 'PCM': [[1, 3.0], [0.3333333333333333, 1]], 'IdLevel': 1, 'LocalW': [0.75, 0.25], 'GlobW': 1},
##"E1_1": {'Name': 'Child No 1 of the E0_0', 'Parents': ['E0_0'], 'About': 'About E1_1', 'Childs': ['E2_2', 'E2_3'], 'PCM': [[1, 0.3333333333], [3.0000000003, 1]], 'IdLevel': 1, 'LocalW': [0.24999999998125, 0.75000000001875], 'GlobW': 1},
##"E2_0": {'Name': 'Child No 0 of the E1_0', 'Parents': ['E1_0'], 'About': 'About E2_0', 'Childs': ['E3_0', 'E3_1', 'E3_2'], 'PCM': [[1, 0.33333333333333, 1.0], [3.00000000000003, 1, 3.0], [1.0, 0.3333333333333333, 1]], 'IdLevel': 2, 'LocalW': [0.19999999999999904, 0.6000000000000012, 0.19999999999999973], 'GlobW': 1},
##"E2_1": {'Name': 'Child No 1 of the E1_0', 'Parents': ['E1_0'], 'About': 'About E2_1', 'Childs': ['E3_0', 'E3_1', 'E3_2'], 'PCM': [[1, 0.5, 0.25], [2.0, 1, 0.5], [4.0, 2.0, 1]], 'IdLevel': 2, 'LocalW': [0.14285714285714288, 0.28571428571428564, 0.57142857142857151], 'GlobW': 1},
##"E2_2": {'Name': 'Child No 2 of the E1_1', 'Parents': ['E1_1'], 'About': 'About E2_2', 'Childs': ['E3_0', 'E3_1', 'E3_2'], 'PCM': [[1, 2.0, 2.0], [0.5, 1, 1.0], [0.5, 1.0, 1]], 'IdLevel': 2, 'LocalW': [0.5, 0.24999999999999994, 0.25], 'GlobW': 1},
##"E2_3": {'Name': 'Child No 3 of the E1_1', 'Parents': ['E1_1'], 'About': 'About E2_3', 'Childs': ['E3_0', 'E3_1', 'E3_2'], 'PCM': [[1, 0.3333333333, 0.3333333333], [3.0000000003, 1, 1.0], [3.0000000003, 1.0, 1]], 'IdLevel': 2, 'LocalW': [0.14285714284489795, 0.42857142857755098, 0.42857142857755104], 'GlobW': 1},
##"E3_0": {'Name': 'Child No 0 of the E2_3', 'Parents': ['E2_0', 'E2_1', 'E2_2', 'E2_3'], 'About': 'About E3_0', 'Childs': [], 'PCM': [], 'IdLevel': 3, 'LocalW': [], 'GlobW': 1},
##"E3_1": {'Name': 'Child No 1 of the E2_3', 'Parents': ['E2_0', 'E2_1', 'E2_2', 'E2_3'], 'About': 'About E3_1', 'Childs': [], 'PCM': [], 'IdLevel': 3, 'LocalW': [], 'GlobW': 1},
##"E3_2": {'Name': 'Child No 2 of the E2_3', 'Parents': ['E2_0', 'E2_1', 'E2_2', 'E2_3'], 'About': 'About E3_2', 'Childs': [], 'PCM': [], 'IdLevel': 3, 'LocalW': [], 'GlobW': 1}}

# Test No 2_1
##Hier = {"E0_0": {'Name': 'Root', 'Parents': [], 'About': 'This is a main goal', 'Childs': ['E1_0', 'E1_1'], 'PCM': [[1, 2.0], [0.5, 1]], 'IdLevel': 0, 'LocalW': [0.66666666666666663, 0.33333333333333331], 'GlobW': 1},
##"E1_0": {'Name': 'Child No 0 of the E0_0', 'Parents': ['E0_0'], 'About': 'About E1_0', 'Childs': ['E2_0', 'E2_1'], 'PCM': [[1, 3.0], [0.3333333333333333, 1]], 'IdLevel': 1, 'LocalW': [0.75, 0.25], 'GlobW': 1},
##"E1_1": {'Name': 'Child No 1 of the E0_0', 'Parents': ['E0_0'], 'About': 'About E1_1', 'Childs': ['E2_2', 'E2_3'], 'PCM': [[1, 0.3333333333], [3.0000000003, 1]], 'IdLevel': 1, 'LocalW': [0.24999999998125, 0.75000000001875], 'GlobW': 1},
##"E1_2": {'Name': 'Child No 2 of the E0_0', 'Parents': ['E0_0'], 'About': 'About E1_2', 'Childs': ['E2_2', 'E2_3'], 'PCM': [[1, 0.3333333333], [3.0000000003, 1]], 'IdLevel': 1, 'LocalW': [0.24999999998125, 0.75000000001875], 'GlobW': 1},
##"E2_0": {'Name': 'Child No 0 of the E1_0', 'Parents': ['E1_0'], 'About': 'About E2_0', 'Childs': ['E3_0', 'E3_1', 'E3_2'], 'PCM': [[1, 0.33333333333333, 1.0], [3.00000000000003, 1, 3.0], [1.0, 0.3333333333333333, 1]], 'IdLevel': 2, 'LocalW': [0.19999999999999904, 0.6000000000000012, 0.19999999999999973], 'GlobW': 1},
##"E2_1": {'Name': 'Child No 1 of the E1_0', 'Parents': ['E1_0'], 'About': 'About E2_1', 'Childs': ['E3_0', 'E3_1', 'E3_2'], 'PCM': [[1, 0.5, 0.25], [2.0, 1, 0.5], [4.0, 2.0, 1]], 'IdLevel': 2, 'LocalW': [0.14285714285714288, 0.28571428571428564, 0.57142857142857151], 'GlobW': 1},
##"E2_2": {'Name': 'Child No 2 of the E1_1', 'Parents': ['E1_1','E1_2'], 'About': 'About E2_2', 'Childs': ['E3_0', 'E3_1', 'E3_2'], 'PCM': [[1, 2.0, 2.0], [0.5, 1, 1.0], [0.5, 1.0, 1]], 'IdLevel': 2, 'LocalW': [0.5, 0.24999999999999994, 0.25], 'GlobW': 1},
##"E2_3": {'Name': 'Child No 3 of the E1_1', 'Parents': ['E1_1','E1_2'], 'About': 'About E2_3', 'Childs': ['E3_0', 'E3_1', 'E3_2'], 'PCM': [[1, 0.3333333333, 0.3333333333], [3.0000000003, 1, 1.0], [3.0000000003, 1.0, 1]], 'IdLevel': 2, 'LocalW': [0.14285714284489795, 0.42857142857755098, 0.42857142857755104], 'GlobW': 1},
##"E3_0": {'Name': 'Child No 0 of the E2_3', 'Parents': ['E2_0', 'E2_1', 'E2_2', 'E2_3'], 'About': 'About E3_0', 'Childs': [], 'PCM': [], 'IdLevel': 3, 'LocalW': [], 'GlobW': 1},
##"E3_1": {'Name': 'Child No 1 of the E2_3', 'Parents': ['E2_0', 'E2_1', 'E2_2', 'E2_3'], 'About': 'About E3_1', 'Childs': [], 'PCM': [], 'IdLevel': 3, 'LocalW': [], 'GlobW': 1},
##"E3_2": {'Name': 'Child No 2 of the E2_3', 'Parents': ['E2_0', 'E2_1', 'E2_2', 'E2_3'], 'About': 'About E3_2', 'Childs': [], 'PCM': [], 'IdLevel': 3, 'LocalW': [], 'GlobW': 1}}


# Test No 2_2
##Hier = {"E0_0": {'Name': 'Root', 'Parents': [], 'About': 'This is a main goal', 'Childs': ['E1_0', 'E1_1'], 'PCM': [[1, 2.0], [0.5, 1]], 'IdLevel': 0, 'LocalW': [0.66666666666666663, 0.33333333333333331], 'GlobW': 1},
##"E1_0": {'Name': 'Child No 0 of the E0_0', 'Parents': ['E0_0'], 'About': 'About E1_0', 'Childs': ['E2_0', 'E2_1'], 'PCM': [[1, 3.0], [0.3333333333333333, 1]], 'IdLevel': 1, 'LocalW': [0.75, 0.25], 'GlobW': 1},
##"E1_1": {'Name': 'Child No 1 of the E0_0', 'Parents': ['E0_0'], 'About': 'About E1_1', 'Childs': ['E2_2', 'E2_3'], 'PCM': [[1, 0.3333333333], [3.0000000003, 1]], 'IdLevel': 1, 'LocalW': [0.24999999998125, 0.75000000001875], 'GlobW': 1},
##"E1_2": {'Name': 'Child No 2 of the E0_0', 'Parents': ['E0_0'], 'About': 'About E1_2', 'Childs': ['E2_4', 'E2_5'], 'PCM': [[1, 0.3333333333], [3.0000000003, 1]], 'IdLevel': 1, 'LocalW': [0.24999999998125, 0.75000000001875], 'GlobW': 1},
##"E2_0": {'Name': 'Child No 0 of the E1_0', 'Parents': ['E1_0'], 'About': 'About E2_0', 'Childs': ['E3_0', 'E3_1', 'E3_2'], 'PCM': [[1, 0.33333333333333, 1.0], [3.00000000000003, 1, 3.0], [1.0, 0.3333333333333333, 1]], 'IdLevel': 2, 'LocalW': [0.19999999999999904, 0.6000000000000012, 0.19999999999999973], 'GlobW': 1},
##"E2_1": {'Name': 'Child No 1 of the E1_0', 'Parents': ['E1_0'], 'About': 'About E2_1', 'Childs': ['E3_0', 'E3_1', 'E3_2'], 'PCM': [[1, 0.5, 0.25], [2.0, 1, 0.5], [4.0, 2.0, 1]], 'IdLevel': 2, 'LocalW': [0.14285714285714288, 0.28571428571428564, 0.57142857142857151], 'GlobW': 1},
##"E2_2": {'Name': 'Child No 2 of the E1_1', 'Parents': ['E1_1'], 'About': 'About E2_2', 'Childs': ['E3_0', 'E3_1', 'E3_2'], 'PCM': [[1, 2.0, 2.0], [0.5, 1, 1.0], [0.5, 1.0, 1]], 'IdLevel': 2, 'LocalW': [0.5, 0.24999999999999994, 0.25], 'GlobW': 1},
##"E2_3": {'Name': 'Child No 3 of the E1_1', 'Parents': ['E1_1'], 'About': 'About E2_3', 'Childs': ['E3_0', 'E3_1', 'E3_2'], 'PCM': [[1, 0.3333333333, 0.3333333333], [3.0000000003, 1, 1.0], [3.0000000003, 1.0, 1]], 'IdLevel': 2, 'LocalW': [0.14285714284489795, 0.42857142857755098, 0.42857142857755104], 'GlobW': 1},
##"E2_4": {'Name': 'Child No 4 of the E1_2', 'Parents': ['E1_2'], 'About': 'About E2_4', 'Childs': ['E3_0', 'E3_1', 'E3_2'], 'PCM': [[1, 2.0, 2.0], [0.5, 1, 1.0], [0.5, 1.0, 1]], 'IdLevel': 2, 'LocalW': [0.5, 0.24999999999999994, 0.25], 'GlobW': 1},
##"E2_5": {'Name': 'Child No 5 of the E1_2', 'Parents': ['E1_2'], 'About': 'About E2_5', 'Childs': ['E3_0', 'E3_1', 'E3_2'], 'PCM': [[1, 0.3333333333, 0.3333333333], [3.0000000003, 1, 1.0], [3.0000000003, 1.0, 1]], 'IdLevel': 2, 'LocalW': [0.14285714284489795, 0.42857142857755098, 0.42857142857755104], 'GlobW': 1},
##"E3_0": {'Name': 'Child No 0 of the E2_3', 'Parents': ['E2_0', 'E2_1', 'E2_2', 'E2_3', 'E2_4', 'E2_5'], 'About': 'About E3_0', 'Childs': [], 'PCM': [], 'IdLevel': 3, 'LocalW': [], 'GlobW': 1},
##"E3_1": {'Name': 'Child No 1 of the E2_3', 'Parents': ['E2_0', 'E2_1', 'E2_2', 'E2_3', 'E2_4', 'E2_5'], 'About': 'About E3_1', 'Childs': [], 'PCM': [], 'IdLevel': 3, 'LocalW': [], 'GlobW': 1},
##"E3_2": {'Name': 'Child No 2 of the E2_3', 'Parents': ['E2_0', 'E2_1', 'E2_2', 'E2_3', 'E2_4', 'E2_5'], 'About': 'About E3_2', 'Childs': [], 'PCM': [], 'IdLevel': 3, 'LocalW': [], 'GlobW': 1}}


# Test No 3
##Hier = {"E0_0": {'Parents': [], 'PCM': [[1, 2.0], [0.5, 1]], 'Childs': ['E1_0', 'E1_1'], 'GlobW': 1, 'About': 'This is a main goal', 'Name': 'Root', 'LocalW': [0.66666666666666663, 0.33333333333333331], 'IdLevel': 0},
##"E1_0": {'Parents': ['E0_0'], 'PCM': [[1, 0.33333333333, 1.0], [3.00000000003, 1, 3.0], [1.0, 0.3333333333333333, 1]], 'Childs': ['E2_0', 'E2_1', 'E2_2'], 'GlobW': 1, 'About': 'About E1_0', 'Name': 'Child No 0 of the E0_0', 'LocalW': [0.19999999999906665, 0.60000000000120013, 0.19999999999973331], 'IdLevel': 1},
##"E1_1": {'Parents': ['E0_0'], 'PCM': [[1, 0.5, 0.25], [2.0, 1, 0.5], [4.0, 2.0, 1]], 'Childs': ['E2_0', 'E2_1', 'E2_2'], 'GlobW': 1, 'About': 'About E1_1', 'Name': 'Child No 1 of the E0_0', 'LocalW': [0.14285714285714288, 0.28571428571428564, 0.57142857142857151], 'IdLevel': 1},
##"E2_0": {'Parents': ['E1_1', 'E1_0'], 'PCM': [], 'Childs': [], 'GlobW': 1, 'About': 'About E2_0', 'Name': 'Child No 0 of the E1_1', 'LocalW': [], 'IdLevel': 2},
##"E2_1": {'Parents': ['E1_1', 'E1_0'], 'PCM': [], 'Childs': [], 'GlobW': 1, 'About': 'About E2_1', 'Name': 'Child No 1 of the E1_1', 'LocalW': [], 'IdLevel': 2},
##"E2_2": {'Parents': ['E1_1', 'E1_0'], 'PCM': [], 'Childs': [], 'GlobW': 1, 'About': 'About E2_2', 'Name': 'Child No 2 of the E1_1', 'LocalW': [], 'IdLevel': 2}}

# Test No 3_1
##Hier = {"E0_0": {'Parents': [], 'PCM': [[1, 2.0], [0.5, 1]], 'Childs': ['E1_0', 'E1_1'], 'GlobW': 1, 'About': 'This is a main goal', 'Name': 'Root', 'LocalW': [], 'IdLevel': 0},
##"E1_0": {'Parents': ['E0_0'], 'PCM': [[1, 0.33333333333, 1.0], [3.00000000003, 1, 3.0], [1.0, 0.3333333333333333, 1]], 'Childs': ['E2_0', 'E2_1', 'E2_2'], 'GlobW': 1, 'About': 'About E1_0', 'Name': 'Child No 0 of the E0_0', 'LocalW': [], 'IdLevel': 1},
##"E1_1": {'Parents': ['E0_0'], 'PCM': [[1, 0.5, 0.25], [2.0, 1, 0.5], [4.0, 2.0, 1]], 'Childs': ['E2_0', 'E2_1', 'E2_2'], 'GlobW': 1, 'About': 'About E1_1', 'Name': 'Child No 1 of the E0_0', 'LocalW': [], 'IdLevel': 1},
##"E2_0": {'Parents': ['E1_1', 'E1_0'], 'PCM': [], 'Childs': [], 'GlobW': 1, 'About': 'About E2_0', 'Name': 'Child No 0 of the E1_1', 'LocalW': [], 'IdLevel': 2},
##"E2_1": {'Parents': ['E1_1', 'E1_0'], 'PCM': [], 'Childs': [], 'GlobW': 1, 'About': 'About E2_1', 'Name': 'Child No 1 of the E1_1', 'LocalW': [], 'IdLevel': 2},
##"E2_2": {'Parents': ['E1_1', 'E1_0'], 'PCM': [], 'Childs': [], 'GlobW': 1, 'About': 'About E2_2', 'Name': 'Child No 2 of the E1_1', 'LocalW': [], 'IdLevel': 2}}


Hier = INPUT.buildHierarchy()
# Строку Hier = INPUT.buildHierarchy() необходимо закоментарить, если использовать один из тестов приведенных выше

Hier = INPUT.inputAllPCMs(Hier)

INPUT.displayHier(Hier)

methodLocWs = INPUT.chooseMethod(["EM", "RGMM", "AN"])

Hier = CALC.findAllLocalWs(Hier, methodLocWs)

print("***********************************************************")
print("Results with local weights of all elements of the hierarchy")
INPUT.displayHier(Hier)

##methodGlobWs = INPUT.chooseMethod(["Distributive", "Ideal", "Multiplicative", "MaxMin"])

methodGlobWs = "Distributive"

Hier = CALC.findAllGlobalWs(Hier, methodGlobWs)

print("************************************************************")
print("Results with global weights of all elements of the hierarchy")
INPUT.displayHier(Hier)


#*************************************************************************************
##alternLocWs=[[2.52, 0.38], [0.31, 2.29], [1.26, 1.15]]
##critGlobWs=[0.6, 0.4]
##
##print("Глобальные веса множества альтернатив:")
##print("Дистрибутивный метод: "+ str(GLOB_W.DistributiveMethod(alternLocWs, critGlobWs)))
##print("Идеальный метод: "+ str(GLOB_W.IdealMethod(alternLocWs, critGlobWs)))
##print("Мультипликативный метод: "+ str(GLOB_W.MultiplicativeMethod(alternLocWs, critGlobWs)))
##print("Максиминный метод: "+ str(GLOB_W.MaxMinMethod(alternLocWs,critGlobWs)))
###print("Метод ГУБОПА: "+ str(GLOB_W.normalizationWs(GLOB_W.GroupBinaryPrefMethod(PCM))))

