import structureHierachy as STRUCT_HIER


def selectSourceElem(Hier):

    listElemsHier=sorted(Hier) #список всех элементов в иерархии
                               #отсортированный список всех ключей словаря Hier

    print(listElemsHier)

    if len(listElemsHier)>0:

        CRIT = input() # эл-т, к которому добавляем дочерний
        errorCode = 1

        while not (CRIT in listElemsHier):
            print("Select a source element from the following list:")
            print(listElemsHier)
            CRIT = input()
            errorCode = 1
    else:
        print("Иерархия пуста")
        CRIT = str()
        errorCode = 0

    return {"CRIT":CRIT, "errorCode":errorCode}



def selectChildElem(Hier,CRIT):

    levelChildsCRIT = Hier[CRIT]["IdLevel"] + 1 # номер уровня дочерних элементов эл-та CRIT

    candChildsCRIT = list()
    for key in Hier:
        if Hier[key]["IdLevel"] >= levelChildsCRIT: # в список кандидатов в дочерние эл-ты для эл-та CRIT
            candChildsCRIT.append(key) # записываем уже существующие эл-ты дочернего уровня, 
                                         # а также уже существующие эл-ты всех нижележащих уровней

    print("Select a child element for the element "+ CRIT +" from the following list of existing elements:")
    print(candChildsCRIT)

    childCRIT =input()

    return childCRIT 

    
    
def buildHierarchy():

    Hier = STRUCT_HIER.addRoot()

    print()
    for key in sorted(Hier):
        print(key, Hier[key])

    print()
    print("Do you want to add a new link (y/n)?")

    while input()=="y":

        print()
        print("Select a source element from the following list to add new link from this element (new child elem):")

        res = selectSourceElem(Hier)
        
        CRIT = res["CRIT"]
        errorCode = res["errorCode"]

        if errorCode:

            childCRIT = selectChildElem(Hier,CRIT)
            

            levelChildsCRIT = Hier[CRIT]["IdLevel"] + 1 # номер уровня дочерних элементов эл-та CRIT

            candChildsCRIT = list()
            for key in Hier:
                if Hier[key]["IdLevel"] >= levelChildsCRIT: # в список кандидатов в дочерние эл-ты для эл-та CRIT
                    candChildsCRIT.append(key) # записываем уже существующие эл-ты дочернего уровня, 
                                                 # а также уже существующие эл-ты всех нижележащих уровней


            if childCRIT in candChildsCRIT:  # если дочерний эл-т childCRIT для эл-та CRIT находится в списке кандидатов,
                                             # то надо только установить связь наследования

                res = STRUCT_HIER.addNewLink(Hier, CRIT, childCRIT) # создает связь (ребро) от CRIT к childCRIT

                Hier = res["Hier"]
                errorCode = res["errorCode"] 

                if errorCode == 1:
                    print()
                    for key in sorted(Hier):
                        print(key, Hier[key])

                elif errorCode == 0:
                    print("Такой эл-т уже есть среди дочерних эл-тов "+ CRIT)

            else:
                print("New element is constructed ...")
                Hier = STRUCT_HIER.addNewChildElem(Hier, CRIT)  # создает новый эл-т и добавляет его в иерархию как дочерний к выбранному эл-ту CRI
                
                print()
                for key in sorted(Hier):
                    print(key, Hier[key])
        else:
            print("Иерархия пуста")

        print()
        print("Do you want to add a new link (y/n)?")


    Hier = STRUCT_HIER.sortChilds(Hier)
    Hier = STRUCT_HIER.sortParents(Hier)

    print()
    for key in sorted(Hier):
        print(key, Hier[key])


    return Hier




def inputPCM(objects):

    N = len(objects)
    PCM=list()
    
    if N>0:
        for i in range(N):
            PCM.append([])
            for j in range(N):
                PCM[i].append(1)   

        # заполнение элементов PCM с клавиатуры
        for i in range(N):
            for j in range(N):
                if j>i:
                    print("Please compare "+objects[i]+" and "+objects[j])
                    value=float(input())
                    PCM[i][j]=value
                    PCM[j][i]=1/value
        errorCode = 1
    else:
        errorCode = 0 # print("Список objects не содержит дочерних элементов. PCM пуста")


    return {"PCM":PCM, "errorCode":errorCode}



def inputPCM_oneCrit(Hier, CRIT):
# определение PCM на основании списка дочерних элементов ВЫБРАННОГО ЭЛЕМЕНТА CRIT
# N-длина списка дочерних элементов выбранного элемента CRIT
# PCM-матрица NxN
# по умолчанию инициализируем элементы PCM единицами

##    print()
##    print("Select an element from the following list (criteria) to build a PCM in terms of this elem:")
##
##    res = selectSourceElem(Hier)
##    
##    CRIT = res["CRIT"]
##    errorCode2 = res["errorCode"]
    errorCode2 = 1

    errorCode = 0

    if errorCode2:

        objects = Hier[ CRIT ]["Childs"] # список дочерних эл-тов для выбранного эл-та CRIT

        print("Сравните следующие элементы относительно элемента " +CRIT)
        res = inputPCM(objects)
    
        PCM = res["PCM"]
        errorCode2 = res["errorCode"]

        if errorCode2:           
            
            Hier[ CRIT ]["PCM"]=PCM # обновляем соответствующее поле PCM для выбранного элемента CRIT

            errorCode = 1
            print("PCM of child elements of the element "+CRIT)
            print(Hier[ CRIT ]["PCM"])

        else:
            errorCode = 0
            print("Элемент "+ CRIT +" не содержит дочерних элементов. PCM пуста")
            print(Hier[ CRIT ]["PCM"])

    else:
        print("Иерархия пуста")


    print()
    for key in sorted(Hier):
        print(key, Hier[key])
        

    return Hier




def inputAllPCMs(Hier):

    for key in Hier:
        if len(Hier[key]["Childs"]) > 0:

            if Hier[key]["PCM"] ==[]:
                Hier = inputPCM_oneCrit(Hier, key)
            

    return Hier


def showPCM_oneCrit(Hier, CRIT): # вывод матрицы МПС элементов относительно эл-та CRIT

    errorCode = 0

    if Hier[CRIT]["Childs"] != []:

        if Hier[ CRIT ]["PCM"] != [] :           
            print("МПС элементов " + Hier[CRIT]["Childs"] + " относительно элемента "+CRIT)
            print(Hier[ CRIT ]["PCM"])
            errorCode = 1

        else:
            print("МПС дочерних элементов "+ CRIT +" не заполнена")
            errorCode = 0

    else:
        print("Элемент "+ CRIT +" не содержит дочерних элементов")
        errorCode = 2
        

    return errorCode



def showLocalWs_oneCrit(Hier, CRIT): # вывод вектора локальных весов элементов относительно эл-та CRIT

    errorCode = 0

    if Hier[CRIT]["Childs"] != []:

        if Hier[ CRIT ]["LocalW"] != [] :           
            print("Вектор локальных весов элементов " + Hier[CRIT]["Childs"] + " относительно элемента "+CRIT)
            print(Hier[ CRIT ]["LocalW"])
            errorCode = 1

        else:
            print("Вектор локальных весов дочерних элементов "+ CRIT +" не вычислен")
            errorCode = 0

    else:
        print("Элемент "+ CRIT +" не содержит дочерних элементов")
        errorCode = 2
        

    return errorCode



def showVector(v, names):
   N = len(v)
   for i in range(N):
       print(names[i] + "   " +str(v[i]))

    


def chooseMethod(methodsList):
    
    print("Choose method from the list ")
    print(methodsList)

    method = input()

    while not (method in methodsList):
        
        print("Choose method from the list ")
        print(methodsList)

        method = input()
    
    return method




def displayHier(Hier):

    print()
    for key in sorted(Hier):
        print(key, Hier[key])




def showKeysByLevels(Hier): #вывод ключей эл-тов по уровням

    #вычисление глубины иерархии (номера последнего уровня)
    maxLevel = 0
    for key in Hier:
        if Hier[key]["IdLevel"] > maxLevel:
            maxLevel = Hier[key]["IdLevel"]

    #print("The hierarchy has "+str(maxLevel + 1)+" levels")

    #вывод ключей эл-тов по уровням
    for iLevel in range(0,maxLevel+1): # фиксируем номер уровня
        print()
        print(str(iLevel))
        for key in Hier:
            if Hier[key]["IdLevel"] == iLevel: # фиксируем эл-т key этого уровня
                print(key)
