#НЕПОЛНАЯ иерархия с произвольным количеством уровней

def addRoot():

    IdElem = str() # ключ эл-та
    Name = str()  # имя эл-та, отображаемое при графической прорисовке иерархии в интерфейсе
    About = str() # описание эл-та
    Childs = list()  # список ключей дочерних элементов данного эл-та
    PCM = list() # МПС дочерних элементов данного эл-та
    Parents = list() # список ключей родительских элементов данного эл-та
    IdLevel = int() # номер уровня, к которому принадлежит данный эл-т
    LocalW = list() # вектор локальных весов дочерних элементов данного эл-та
    GlobW = int() # значение глобального веса данного эл-та

    Elem=dict() 
    IdElem = "E0_0"
    Name = "Root"
    About = "This is a main goal"
    IdLevel = 0
    GlobW = 1
    Elem[IdElem]={"Name":Name, "About":About, "Childs":Childs, "PCM":PCM, "Parents":Parents, "IdLevel":IdLevel, "LocalW":LocalW, "GlobW":GlobW}
    #ключ этого словаря - IdElem

    Hier=dict(Elem)     #добавляем в иерархию корень


    return Hier




def addNewChildElem(Hier, CRIT): # создает новый эл-т и добавляет его в иерархию как дочерний к выбранному эл-ту CRIT

    #поля нового элемента
    IdLevel = Hier[CRIT]["IdLevel"] + 1 # номер уровня дочерних эл-тов эл-та CRIT на единицу превышает номер уровня у эл-та CRIT

    # ищем порядковый номер нового эл-та 
    Elems_IdLevel = list() # список эл-тов, уже существующих на уровне IdLevel
    for key in Hier:
        if Hier[key]["IdLevel"] == IdLevel: # ищем эл-ты, кот.уже есть на уровне IdLevel
            Elems_IdLevel.append(key) # записываем эти эл-ты в список 

    num_CRIT = len(Elems_IdLevel)  #порядковый номер нового эл-та = кол-ву существующих эл-тов уровня IdLevel 

    IdElem = "E"+str(IdLevel)+"_"+str(num_CRIT)
    Name = "Child No "+str(num_CRIT)+" of the "+ CRIT
    About = "About "+IdElem
    Childs = list() #список ключей дочерних элементов нового элемента вначале пуст
    PCM = list() # МПС дочерних элементов
    Parents = list()
    Parents.append (CRIT) # родительским эл-том для нового эл-та явл CRIT

    LocalW = list()
    GlobW = None 
    
    Elem=dict() 
    Elem[IdElem]={"Name":Name, "About":About, "Childs":Childs, "PCM":PCM, "Parents":Parents, "IdLevel":IdLevel, "LocalW":LocalW, "GlobW":GlobW}

    #добавляем в иерархию новый элемент - словарь с ключем IdElem
    Hier[IdElem]={"Name":Name, "About":About, "Childs":Childs, "PCM":PCM, "Parents":Parents, "IdLevel":IdLevel, "LocalW":LocalW, "GlobW":GlobW}
    
    print ("New child elem is added:")
    print(Elem)

    # изменяем список ключей дочерних элементов эл-та CRIT
    # добавляем в этот список один новый элемент - ключ IdElem
    Hier[ CRIT ]["Childs"].append(IdElem)


##            print()
##            for key in sorted(Hier):
##                print(key, Hier[key])


    return Hier




def addNewLink(Hier, CRIT, childCRIT): # добавляет новую связь (ребро дерева) от эл-та CRIT к его дочернему эл-ту child_CRIT 

    if not(childCRIT in Hier[ CRIT]["Childs"]): #если такого эл-та еще нет в списке дочерних для CRIT изменяем список ключей дочерних элементов эл-та CRIT
        Hier[ CRIT ]["Childs"].append(childCRIT) # добавляем в этот список один новый элемент - ключ childCRIT

        Hier[ childCRIT ]["Parents"].append(CRIT) # изменяем список ключей родительских элементов эл-та childCRIT, добавляем в этот список один новый элемент - ключ CRIT

        errorCode = 1 # OK
##            print()
##            for key in sorted(Hier):
##                print(key, Hier[key])

    else: errorCode = 0  #print("Такой эл-т уже есть среди дочерних эл-тов "+ CRIT)


    return {"Hier":Hier, "errorCode":errorCode}




def lengthHier(Hier):

    #вычисление глубины иерархии (номера последнего уровня)
    maxLevel = 0
    for key in Hier:
        if Hier[key]["IdLevel"] > maxLevel:
            maxLevel = Hier[key]["IdLevel"]

    print("The hierarchy has "+str(maxLevel + 1)+" levels")
    
    return maxLevel




def deleteElemInList(listElems, name): # в списке listElems удалить эл-т с именем name

    for i in range(len(listElems)):
        if listElems[i] == name:
            listElems[i:(i+1)] = [] # если срезу присвоить пустой список, то эл-ты, попавшие в срез, будут удалены
            break
            
    return listElems




def deleteNode(Hier,CRIT): # удаляет узел CRIT, если он не корень
                           # удаляет внутренний узел или лист

    if (CRIT == "Root") and (lengthHier(Hier) > 0):
        errorCode = 0 #printf("Попытка удалить корень")

    if (CRIT == "Root") and (lengthHier(Hier) == 0): 
        Hier = []

    if (Hier[CRIT]["IdLevel"] > 0):

        CRIT_childs = Hier[CRIT]["Childs"]
        
        for key in CRIT_childs:            
            listElems = deleteElemInList(Hier[key]["Parents"], CRIT) # в списке Hier[key]["Parents"] удалить эл-т с именем CRIT
            if listElems != []:
                Hier[key]["Parents"] = listElems
            else:
                errorCode = 0 # print("Ошибка: эту вершину нельзя удалить, так как она явл. корнем поддерева")                


        for key in CRIT_childs:
            if Hier[key]["Parents"] == []:   # для НЕПОЛНОЙ иерархии при удалении корня поддерева

                errorCode = 0 # print("Ошибка: эту вершину нельзя удалить, так как она явл. корнем поддерева")  

##                Hier[key]["Parents"] = Hier[CRIT]["Parents"]
##
##                for elem in Hier[CRIT]["Parents"]:
##                    Hier[elem]["Childs"].append(key)
                # ДОДЕЛАТЬ извлечение поддерева
                # ДОДЕЛАТЬ случай удаления корня поддерева
                
        
        for node in Hier:
            if CRIT in Hier[node]["Childs"]:
                listElems = deleteElemInList(Hier[node]["Childs"], CRIT) # в списке Hier[node]["Childs"] удалить эл-т с именем CRIT
                Hier[node]["Childs"] = listElems
                Hier[node]["PCM"] = [] # у родительского эл-та node обнуляем МПС (так как у него изменился список дочерних эл-тов)
                Hier[node]["LocalW"] = [] # у родительского эл-та node обнуляем вектор локальных весов (так как у него изменился список дочерних эл-тов)          

        for key in Hier:
            if Hier[key]["IdLevel"] > Hier[CRIT]["IdLevel"]:
                Hier[key]["GlobW"] = None   # сбрасываем глобальные веса эл-тов, которые находятся на нижележащих уровнях по сравнению с удаленным эл-том CRIT
                                            # НЕ совсем КОРРЕКТНО для неполной иерархии (удаляет часть правильной инфы)


        del Hier[CRIT] #удаляем из словаря эл-т с ключем (именем) CRIT             
      

        errorCode=1
        
    return Hier




def isFeedback(Hier, elem1, elem2): # проверка ребра от elem1 к elem2; flag=True если есть обратная связь
                                    # (elem1 на более низком уровне чем elem2)
    flag = False

    if Hier[elem1]["IdLevel"] >= Hier[elem2]["IdLevel"]:
        flag = True
        
    return flag



def isLoop(Hier, elem1, elem2): # flag=True если есть петля (elem1 на том же уровне что и elem2)
    flag = False

    if Hier[elem1]["IdLevel"] == Hier[elem2]["IdLevel"]:
        flag = True
        
    return flag



def sortChilds(Hier):

    for key in Hier:
        childs = Hier[key]["Childs"]
        Hier[key]["Childs"] = sorted(childs)

    return Hier



def sortParents(Hier):

    for key in Hier:
        parents = Hier[key]["Parents"]
        Hier[key]["Parents"] = sorted(parents)

    return Hier 
