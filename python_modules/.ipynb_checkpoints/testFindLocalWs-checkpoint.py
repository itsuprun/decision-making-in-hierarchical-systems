#import interfaceKeyboard as INPUT

import checkCorrectness as CORRECT
import findConsistencyIndexesPCM as CONS_INDEX
import checkConsistencyPCM as CONS
import findUFOs as UFO
import findImprovedPCM as IMPROV_PCM
import findLocalWs as LOC_W
import findLocalWsIPCM as LOC_W_IPCM
#import rankingIntervalWs as RANK


#PCM = list()

#PCM = [[1,0.5,0.25],[2,1,0.5],[4,2,1]] # consistent
#PCM = [[1,2,2],[0.5,1,1],[0.5,1,1]] # consistent
PCM = [[1,2,3],[0.5,1,1],[1/3,1,1]] # inconsistent
#PCM = [[1,2],[0.5,1]]
#PCM=[1]
#PCM = [[1,2,4,1/8],[1/2,1,2,4],[1/4,1/2,1,2],[8,1/4,1/2,1]] # МПС с выбросом [0][3]
#PCM = [[1,2,4,8],[1/2,1,2,4],[1/4,1/2,1,2],[1/8,1/4,1/2,1]] # согласованная МПС, выброс скорректирован
#PCM = [[1,2,4,2],[1/2,1,2,4],[1/4,1/2,1,2],[1/2,1/4,1/2,1]] # МПС с несогл.эл-том [0][3]
#PCM = [[1,2,4,1],[1/2,1,2,4],[1/4,1/2,1,2],[1,1/4,1/2,1]] # МПС с несогл.эл-том [0][3]


#PCM = [[1,2,6],[1/2,1,9],[1/6,1/9,1]] # inconsistent
print("МПС:")
print(PCM)


###--------------------------------------------------------------------
##PCM = INPUT.inputPCM(["E1","E2","E3"])
##print(PCM)

if not CORRECT.isInverseSymmetric(PCM):
    print("МПС построена некорректно. Не удовлетворяет св-ву обратной симметричности")
    
if not CORRECT.isInFundamentalScale(PCM):
    print("МПС задана не в фундаментальной шкале")
 
#--------------------------------------------------------------------
if CONS.isConsist(PCM):
    print("МПС согласованна по определению")
else:
    print("МПС несогласованна по определению")


if CONS.isWeakConsist(PCM):
    print("МПС слабо согласованна по определению")
else:
    print("МПС слабо несогласованна по определению")

#--------------------------------------------------------------------
print("***************************************************")
print("Показатели согласованности:")
print("CR=",str(CONS_INDEX.ConsistencyRatio(PCM)))
print("GCI=",str(CONS_INDEX.GeometricConsistencyIndex(PCM)))
print("HCR=",str(CONS_INDEX.HarmonicConsistencyRatio(PCM)))


#--------------------------------------------------------------------
print("***************************************************")
print("Критерии допустимой несогласованности:")

if CONS.isConsistCR(PCM):
    print("МПС допустимо несогласованна по показателю CR")
else:
    print("МПС недопустимо несогласованна по показателю CR. МПС требует корректировки")


if CONS.isConsistGCI(PCM):
    print("МПС допустимо несогласованна по показателю GCI")
else:
    print("МПС недопустимо несогласованна по показателю GCI. МПС требует корректировки")


if CONS.isConsistHCR(PCM):
    print("МПС допустимо несогласованна по показателю HCR")
else:
    print("МПС недопустимо несогласованна по показателю HCR. МПС требует корректировки")

#--------------------------------------------------------------------
print("***************************************************")
print("Нахождение наиболее несогласованных эл-тов:")
print("The CI method: " + str(UFO.findUFO_methodCI(PCM)))
print("The method of transitivities: " + str(UFO.findUFO_methodTransitivities(PCM)))
print("The Xi-square method: " + str(UFO.findUFO_methodXiSquares(PCM)))
print("The OutFlow method: " + str(UFO.findUFO_methodOutflow(PCM)))
print("The Modified OutFlow method: " + str(UFO.findUFO_methodModifiedOutflow(PCM)))

#--------------------------------------------------------------------
print("***************************************************")
print("Метод мультипликативной корректировки МПС:")
res = IMPROV_PCM.findMultCorrection(PCM,0.7)
print("Corrected PCM:")
print(res["PCM_cor"])
print("Number of iterations:" + str(res["numIterat"]))
print("Sigma and delta coefs:" + str(res["coefs"]))
print("CR(PCM_cor) = "+ str(CONS_INDEX.ConsistencyRatio(res["PCM_cor"])))
print("***************************************************")
print("Метод аддитивной корректировки МПС:")
res = IMPROV_PCM.findAdditivCorrection(PCM,0.7)
print("Corrected PCM:")
print(res["PCM_cor"])
print("Number of iterations:" + str(res["numIterat"]))
print("Sigma and delta coefs:" + str(res["coefs"]))
print("CR(PCM_cor) = "+ str(CONS_INDEX.ConsistencyRatio(res["PCM_cor"])))


#--------------------------------------------------------------------
print("***************************************************")
print("Локальные веса на основании МПС:")
print("EM: "+ str(LOC_W.normalizationWs(LOC_W.EigenvectorMethod(PCM))))
wEM = LOC_W.EigenvectorMethod(PCM)
print("RGMM: "+ str(LOC_W.normalizationWs(LOC_W.RowGeometricMeanMethod(PCM))))
print("AN: "+ str(LOC_W.normalizationWs(LOC_W.ArithmeticNormalizationMethod(PCM))))
print("StableUFOsMethod:"+ str(LOC_W.normalizationWs(LOC_W.StableUFOsMethod(PCM))))

print("---------------------------------------------------")
print("CrispLUAMmodel: ")
resLUAM = LOC_W.CrispLUAMmodel(PCM)
#print("ChebNorm (CrispLUAMmodel) = " + str(LOC_W.ChebNorm(resLUAM["wL"],resLUAM["wU"])))
#RANK.findRankingPreferenceMethod_2(resLUAM["wL"],resLUAM["wU"])

print("---------------------------------------------------")
print("additivModel: ")
resAModel = LOC_W_IPCM.additivModel(PCM, PCM)
#print("ChebNorm (additivModel) = " + str(LOC_W.ChebNorm(resAModel["wL"],resAModel["wU"])))
#RANK.findRankingPreferenceMethod_2(resAModel["wL"],resAModel["wU"])

