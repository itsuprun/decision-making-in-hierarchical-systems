import structureHierachy as STRUCT_HIER
import checkCorrectness as CORRECT
import findConsistencyIndexesPCM as CONS_INDEX
import checkConsistencyPCM as CONS
import findLocalWs as LOC_W
import findGlobalWs as GLOB_W

import interfaceKeyboard as INPUT


def normalizationWs(w):
    N = len(w)

    normW = w
    
    if N>1:
        normW = list()
        s = 0
        for i in range(N):
            s += w[i]
        for i in range(N):
            normW.append(w[i]/s)

    return normW


def normalizationWsLevel(Hier, iLevel):     # Нормировка глоб.весов всех эл-тов уровня iLevel

    maxLevel = STRUCT_HIER.lengthHier(Hier)

    if iLevel in range(1,maxLevel+1):
        w = list()
        elemsLevel = list()
        for key in Hier:
            if Hier[key]["IdLevel"] == iLevel: # рассматриваем эл-ты key уровня iLevel
                elemsLevel.append(key) 
                w.append(Hier[key]["GlobW"])  # список глоб.весов эл-тов key
                    
        w = normalizationWs(w)
        
        for i in range(len(elemsLevel)):
            key = elemsLevel[i]
            Hier[key]["GlobW"] = w[i]

        errorCode = 1
        
    else:
        errorCode = 0
        print("Error: incorrect number of Level")

    return Hier




def findLocalWs_oneCrit(Hier, CRIT, methodName):

    methodsDict = {"EM":LOC_W.EigenvectorMethod, "RGMM":LOC_W.RowGeometricMeanMethod, "AN":LOC_W.ArithmeticNormalizationMethod}

    errorCode = 0
    
##    print()
##    print("Select an element from the following list (criteria) to find local weights in terms of this element:")
##
##    res = INPUT.selectSourceElem(Hier)
##    
##    CRIT = res["CRIT"]
##    errorCode2 = res["errorCode"]

    errorCode2 = 1
    
    if errorCode2:

        w = list()

        if len(Hier[ CRIT ]["Childs"]) >0 : # если эл-т CRIT имеет хотя бы один дочерний элемент

            PCM = Hier[ CRIT ]["PCM"]

            if len(PCM) > 0: #если PCM для выбранного элемента CRIT заполнена, не пуста

                w = LOC_W.initialization(len(PCM))

                if methodName in methodsDict:
                    w = (methodsDict[methodName])(PCM)
                    errorCode = 1
                else:
                    errorCode = 0 #print("Неизвестный метод вычисления весов")
                                  
        
                w = normalizationWs(w)

                Hier[ CRIT ]["LocalW"] = w
                
                
                print("Weight vector of child elements of the element "+CRIT+ ", method "+methodName)
                print(w)

            else:
                errorCode = 2
                print("Для выбранного эл-та "+ CRIT +" МПС не заполнена")

        else:
            errorCode = 3
            print("Элемент "+ CRIT +" не содержит дочерних элементов.")

    else:
        errorCode = 4
        print("Иерархия пуста")

##    print()
##    for key in sorted(Hier):
##        print(key, Hier[key])
        

    return {"Hier": Hier, "errorCode": errorCode}




def findAllLocalWs(Hier, methodName):

    for key in Hier:
        if len(Hier[key]["Childs"]) > 0:

            if Hier[key]["LocalW"] ==[]:
                res = findLocalWs_oneCrit(Hier, key, methodName)
                Hier = res["Hier"]

    return Hier




def findGlobWsLevel(Hier, iLevel, methodName): # вычисление глоб.весов эл-тов уровня iLevel, используя methodName

    methodsDict = {"Distributive":GLOB_W.DistributiveMethod, "Ideal":GLOB_W.IdealMethod, "Multiplicative":GLOB_W.MultiplicativeMethod, "MaxMin":GLOB_W.MaxMinMethod}


    elemsLevel = list()
    for key in Hier:
        if Hier[key]["IdLevel"] == iLevel: # список ключей эл-тов заданного уровня
            elemsLevel.append(key)  

    elemsLevel = sorted(elemsLevel)
##    print(elemsLevel)


    # список elemsLevel разбиваем на (непересекающиеся) подсписки elems[i] эл-тов, которые имеют общих родителей
    # одновременно формируются списки общих родителей commonParents[i]
    # все эл-ты списка elems[i] имеют общих родителей из списка commonParents[i]
        

    elems = list()
    commonParents = list()
    elem = list()
    elem.append(elemsLevel[0])
    elems.append(elem)
    commonParents.append(Hier[elem[0]]["Parents"])

##    print(elems) # [['E2_0']]
##    print(commonParents) # [['E1_0']]
##    print(elems[0]) #['E2_0']
##    print(commonParents[0]) #['E1_0']

    k = 0 # номер подсписка elems[k] эл-тов, которые имеют общих родителей
    for i in range(1,len(elemsLevel)):
        key = elemsLevel[i]
        if Hier[key]["Parents"] == commonParents[k]:
            elems[k].append(key)
        else:
            k +=1
            elem = list()
            elem.append(elemsLevel[i])
            elems.append(elem)
            commonParents.append(Hier[elem[0]]["Parents"])
            
    for i in range(len(elems)):
        elems[i] = sorted(elems[i])
        commonParents[i] = sorted(commonParents[i])

##    print(elems) # [['E2_0', 'E2_1'], ['E2_2', 'E2_3']] или [['E2_0', 'E2_1', 'E2_2']] или [['E2_0', 'E2_1'], ['E2_2', 'E2_3'], ['E2_4', 'E2_5']]
##    print(commonParents) # [['E1_0'], ['E1_1']]  или  [['E1_0', 'E1_1']] или [['E1_0'], ['E1_1'], ['E1_2']]


    for k in range(len(elems)):
        
        keysList = elems[k]  # агрегирование весов эл-тов из списка keysList относительно родителей keysParentsList
        keysParentsList = commonParents[k]

        keyLocalWs = list()
        keyParentsGlobWs = list()
        
        for j in range(len(keysParentsList)):
            parent = keysParentsList[j]
            keyParentsGlobWs.append(Hier[parent]["GlobW"])


        keyParentsGlobWs = normalizationWs(keyParentsGlobWs)  # нормировка весов критериев


        for i in range(len(keysList)):
            keyLocalWs.append([]) 
            for j in range(len(keysParentsList)):
                parent = keysParentsList[j]
                if Hier[parent]["Childs"][i] == keysList[i]:
                    keyLocalWs[i].append(Hier[parent]["LocalW"][i])

##        print("-----------------------------")
##        print(keysList)
##        print(keysParentsList)
##        print(keyLocalWs)
##        print(keyParentsGlobWs)


        keyGlobW = GLOB_W.initialization(len(keyLocalWs))

        if methodName in methodsDict:
            res = (methodsDict[methodName])(keyLocalWs, keyParentsGlobWs)
            keyGlobW = res["GlobWs"]
            errorCode = 1
        else:
            errorCode = 0 #print("Неизвестный метод вычисления весов")    


        for i in range(len(keysList)): 
            key = keysList[i]
            Hier[key]["GlobW"] = keyGlobW[i]
            

##        print(keyGlobW)



    return Hier




def findAllGlobalWs(Hier, methodName):

##    print()
##    print("Calculation of global weights of ALL elements in the hierarchy")

    # обход иерархии сверху-вниз по уровням, начиная с нулевого уровня (корня)

    maxLevel = STRUCT_HIER.lengthHier(Hier)       

    iLevel = 0 #номер текущего уровня
    Hier["E0_0"]["GlobW"] = 1 # глоб.вес корня всегда равен единице


    for iLevel in range(1,maxLevel+1): # фиксируем номер уровня
        Hier = findGlobWsLevel(Hier, iLevel, methodName)
#        Hier = normalizationWsLevel(Hier, iLevel) # нормируем эл-ты

        
    Hier = normalizationWsLevel(Hier, maxLevel) # нормируем эл-ты последнего уровня

    return Hier


