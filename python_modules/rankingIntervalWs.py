

def findPreferenceValue(la,ua,lb,ub): # возвращает степень преобладания 0<=p<=1
                                      # интервального числа a=[la,ua] над интервальным числом b=[lb,ub]

    if (la == ua) or (lb == ub):           
        if (la > lb):
            prefVal = 1
        if (la == lb):
            prefVal = 1/2            
        if (la < lb):
            prefVal = 0

    if (la != ua) & (lb != ub):
        prefVal = max ( 1 - max ( (ub - la) / (ua - la + ub - lb), 0 ), 0 )
    else:
        if (ua - la + ub - lb == 0):
            pref = 0

##    if (la == ua) & (lb == ub):
##        if (la > lb):
##            prefVal = 1
##        if (la == lb):
##            prefVal = 1/2            
##        if (la < lb):
##            prefVal = 0
    
    return prefVal



def findRankingPreferenceMethod(xList): # метод степеней преобладания для ранжирования N интервальных чисел
                                   # xList - список интервальных чисел, кот. необходимо проранжировать
    N = len(xList)                 # xList[i] - интервал (список из двух значений)

    p = list()    # p - матрица NxN степеней преобладания

    for i in range(N):
        p.append([])
        for j in range(N):
            if i != j:
                p[i].append(findPreferenceValue(xList[i][0],xList[i][1],xList[j][0],xList[j][1]) )
            else:
                p[i].append(1/2)

    prefs = dict()  # ключ словаря - значение степени преобладания p[i], 
                    # значение по ключу - i - номер объекта
    eps = 0.000001
    for k in range(N):
        if sum(p[k]) in prefs:
            prefs[sum(p[k])+eps] = k
        else:
            prefs[sum(p[k])] = k

    ranking = list()  # список номеров объектов, соответствующих отсортированным в порядке убывания степеням преобладания 
    for key in sorted(prefs, reverse=True):
        ranking.append(prefs[key])
        
    print("Matrix P")
    print(p)
    print("Preferences")
    print(prefs)
    print("Sorted preferences")
    print(sorted(prefs, reverse=True))
    print("Ranking of objects")
    print(ranking)

    return ranking


def findRankingPreferenceMethod_2(xL, xU): # метод степеней преобладания для ранжирования N интервальных чисел
                                   # xL, xU - вектора левых и правых концов чисел, кот. необходимо проранжировать
                                   # xL, xU - вектора (списки)
    if len(xL) != len(xU):
        errorCode = 0  # print("Ошибка размерности")

    N = len(xL)
    p = list()    # p - матрица NxN степеней преобладания

    for i in range(N):
        p.append([])
        for j in range(N):
            if i != j:
                p[i].append(findPreferenceValue(xL[i],xU[i],xL[j],xU[j]) )
            else:
                p[i].append(1/2)

    prefs = dict()  # ключ словаря - значение степени преобладания p[i], 
                    # значение по ключу - i - номер объекта
    eps = 0.000001
    for k in range(N):
        if sum(p[k]) in prefs:
            prefs[sum(p[k])+eps] = k
        else:
            prefs[sum(p[k])] = k

    ranking = list()  # список номеров объектов, соответствующих отсортированным в порядке убывания степеням преобладания 
    for key in sorted(prefs, reverse=True):
        ranking.append(prefs[key])
        
##    print("Matrix P")
##    print(p)
##    print("Preferences")
##    print(prefs)
##    print("Sorted preferences")
##    print(sorted(prefs, reverse=True))
##    print("Ranking of objects")
##    print(ranking)

    return ranking




def isPrefered(ranking, i1, i2): # возвращает true, если w[i1] >= w[i2] в ранжировании ranking элементов w[1], w[2],...,w[N] 
                                 # i1, i2 - номера объектов, начиная с нуля
                                 # ranking - ранжирование весов в порядке убывания 
    flag = False
    if ranking.index(i1) <= ranking.index(i2): # w[i1] >= w[i2], если номер объекта i1 меньше номера объекта i2 в ранжировании ranking
        flag = True

    return flag




def isWeakOrderPreservation(PCM, w): # возвращает список пар индексов [i,j], на которых НЕ сохраняется слабый порядок
                                  # если слабый порядок сохраняется для всех пар индексов, то возвращается пустой список
                                  # PCM - МПС NxNx2 (список списков интервалов), w - вектор весов Nx1x2 (список интервалов) 
    if len(PCM) != len(w):
        errorCode = 0  # print("Ошибка размерности")

    rankingWeights = findRankingPreferenceMethod(w)

    indexList = list()

    unit = [1,1]

    for i in range(len(PCM)):
        for j in range(len(PCM)):
            if i != j:
    ##            PCM_ij = list()
                
                PCM_el = PCM[i][j] # каждый из этих эл-тов - список из двух значений - левый и правый концы интервала
                w_i_el = w[i]
                w_j_el = w[j]

    ##            PCM_ij.append(PCM_el)
    ##            PCM_ij.append([1,1])
                
                rankingPCM = findRankingPreferenceMethod([PCM_el, [1,1]])

                if isPrefered(rankingPCM, 0, 1) & (not isPrefered(rankingWeights, i, j) ):
                # не выполняется слабое сохранение порядка
                    indexList.append([i,j])
            
    
    return indexList
    



def isWeakOrderPreservation_2(L,U, wL, wU): # возвращает список пар индексов [i,j], на которых НЕ сохраняется слабый порядок
                                  # если слабый порядок сохраняется для всех пар индексов, то возвращается пустой список
                                  # PCM D=[L,U] - ИМПС, L,U - матрицы NxN (список списков),
                                  # w=[wL, wU] - вектор интервальных весов, wL, wU - вектора Nx1 (списки) 
    if len(L) != len(U):
        errorCode = 0  # print("Ошибка размерности")
    if len(wL) != len(wU):
        errorCode = 0  # print("Ошибка размерности")
    if len(L) != len(wL):
        errorCode = 0  # print("Ошибка размерности")

        
    rankingWeights = findRankingPreferenceMethod_2(wL,wU)

    indexList = list()

    unit = [1,1]
    
    n = len(L)

    for i in range(n):
        for j in range(n):
            if i != j:
                
                IPCM_el = [ L[i][j], U[i][j] ] # каждый из этих эл-тов - список из двух значений - левый и правый концы интервала
                
                rankingPCM = findRankingPreferenceMethod_2(IPCM_el, [1,1])

                if isPrefered(rankingPCM, 0, 1) & (not isPrefered(rankingWeights, i, j) ):
                # не выполняется слабое сохранение порядка
                    indexList.append([i,j])
            
    
    return indexList
