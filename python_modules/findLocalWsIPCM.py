
from scipy.optimize import linprog
import numpy as np


##Minimize: c^T * x
##
##Subject to: A_ub * x <= b_ub
##A_eq * x == b_eq


def isConsistentIPCM(L,U):
    flag = True
    if len(L) != len(U):
        errorCode = 0
        print("Error")
        
    n = len(L)
    
    matr1 = np.zeros((n,n))
    matr2 = np.zeros((n,n))
    for i in range(n):
        for j in range(n):
            l = 0
            for k in range(n):
                matr1[l] = L[i][k]*L[k][j]
                matr2[l] = U[i][k]*U[k][j]
                l += 1
            print(matr1)
            print(matr2)

            if np.max(matr1) > np.min(matr2):
                flag = False

    return flag




##def GPMmodel(L,U):
##
##    if len(L) != len(U):
##        errorCode = 0
##        print("Error")
##        
##    n = len(L)
##
##
##    
##
##    res = linprog(c, A_ub=A, b_ub=b, options={"disp": True})
##    X = res.x
##
##    wL = np.zeros(n)
##    wU = np.zeros(n)
##    for i in range(n):
##        wL[i] = X[i]
##        wU[i] = X[i+n]
##
##
##    print("X = ")
##    print(res.x)
##    print("J* = ")
##    print(res.fun)
##    print("wL = ", end=" ")
##    print(wL)
##    print("wU = ", end=" ")
##    print(wU)
##    
##    return {"J*":res.fun, "wL":wL, "wU":wU}






def LowerLUAMmodel(L,U):

    if len(L) != len(U):
        errorCode = 0
        print("Error")
        
    n = len(L)

    c = - np.hstack( (-np.ones(n), np.ones(n)) )
    print(c)

    A1_1 = np.zeros((n*n, n))
    k = 0
    for i in range(n):
        for j in range(n):
            if i!=j:
                A1_1[k][i] = -1
                k += 1
    A1_2 = np.zeros((n*n, n))
    k = 0
    for i in range(n):
        for j in range(n):
            if i!=j:
                A1_2[k][j] = L[i][j]
                k += 1

    A1 = np.hstack( (A1_1, A1_2 ))                
    b1 = np.zeros(n*n)           
    print("---------------------------")
    print(A1)    
    print("---------------------------")
    print(b1)
    print("---------------------------")


    A2_1 = np.zeros((n*n, n))
    k = 0
    for i in range(n):
        for j in range(n):
            if j!=i:
                A2_1[k][i] = - U[j][i]
                k += 1
    A2_2 = np.zeros((n*n, n))
    k = 0
    for i in range(n):
        for j in range(n):
            if j!=i:
                A2_2[k][j] = 1
                k += 1

    A2 = np.hstack( (A2_1, A2_2 ))                
    b2 = np.zeros(n*n)           
    print("---------------------------")
    print(A2)    
    print("---------------------------")
    print(b2)
    print("---------------------------")               


    A3_1 = -np.eye(n)
    A3_2 = -( np.ones((n,n)) - np.eye(n) )   
    A3 = np.hstack( (A3_1, A3_2 ))
    b3 = - np.ones(n)
            
    print("---------------------------")
    print(A3)    
    print("---------------------------")
    print(b3)
    print("---------------------------")
    
    
    A4_1 = np.ones((n,n)) - np.eye(n)
    A4_2 = np.eye(n)
    A4 =  np.hstack( (A4_1, A4_2 ))    
    b4 = np.ones(n)
            
    print("---------------------------")
    print(A4)    
    print("---------------------------")
    print(b4)
    print("---------------------------")


    A5 = np.hstack( (np.eye(n), -np.eye(n)) )
    b5 = np.zeros(n)
    print("---------------------------")
    print(A5)    
    print("---------------------------")
    print(b5)
    print("---------------------------")

    eps = 0.001
    A6 = np.hstack( (-np.eye(n), np.zeros((n,n))) )
    b6 = - eps*np.ones(n)   
    print("---------------------------")
    print(A6)    
    print("---------------------------")
    print(b6)
    print("---------------------------")
    

    A = np.vstack((A1, A2, A3, A4, A5, A6))
    b = np.hstack((b1, b2, b3, b4, b5, b6))
    print("---------------------------")
    print(A)    
    print("---------------------------")
    print(b)
    print("---------------------------")    
    

    res = linprog(c, A_ub=A, b_ub=b, options={"disp": True})
    X = res.x

    wL = np.zeros(n)
    wU = np.zeros(n)
    for i in range(n):
        wL[i] = X[i]
        wU[i] = X[i+n]


    print("X = ")
    print(res.x)
    print("J* = ")
    print(res.fun)
    print("wL = ", end=" ")
    print(wL)
    print("wU = ", end=" ")
    print(wU)
    
    return {"J*":res.fun, "wL":wL, "wU":wU}





def UpperLUAMmodel(L,U):

    if len(L) != len(U):
        errorCode = 0
        print("Error")
        
    n = len(L)

    c = np.hstack( (-np.ones(n), np.ones(n)) )
    print(c)

    A1_1 = np.zeros((n*n, n))
    k = 0
    for i in range(n):
        for j in range(n):
#            if i!=j:
            A1_1[k][i] = 1
            k += 1
    A1_2 = np.zeros((n*n, n))
    k = 0
    for i in range(n):
        for j in range(n):
##            if i!=j:
            A1_2[k][j] = - L[i][j]
            k += 1

    A1 = np.hstack( (A1_1, A1_2 ))                
    b1 = np.zeros(n*n)           
    print("---------------------------")
    print(A1)    
    print("---------------------------")
    print(b1)
    print("---------------------------")


    A2_1 = np.zeros((n*n, n))
    k = 0
    for i in range(n):
        for j in range(n):
##            if j!=i:
            A2_1[k][i] = U[j][i]
            k += 1
    A2_2 = np.zeros((n*n, n))
    k = 0
    for i in range(n):
        for j in range(n):
##            if j!=i:
            A2_2[k][j] = -1
            k += 1

    A2 = np.hstack( (A2_1, A2_2 ))                
    b2 = np.zeros(n*n)           
    print("---------------------------")
    print(A2)    
    print("---------------------------")
    print(b2)
    print("---------------------------")               


    A3_1 = -np.eye(n)
    A3_2 = -( np.ones((n,n)) - np.eye(n) )   
    A3 = np.hstack( (A3_1, A3_2 ))
    b3 = - np.ones(n)
            
    print("---------------------------")
    print(A3)    
    print("---------------------------")
    print(b3)
    print("---------------------------")
    
    
    A4_1 = np.ones((n,n)) - np.eye(n)
    A4_2 = np.eye(n)
    A4 =  np.hstack( (A4_1, A4_2 ))    
    b4 = np.ones(n)
            
    print("---------------------------")
    print(A4)    
    print("---------------------------")
    print(b4)
    print("---------------------------")


    A5 = np.hstack( (np.eye(n), -np.eye(n)) )
    b5 = np.zeros(n)
    print("---------------------------")
    print(A5)    
    print("---------------------------")
    print(b5)
    print("---------------------------")

    eps = 0.001
    A6 = np.hstack( (-np.eye(n), np.zeros((n,n))) )
    b6 = - eps*np.ones(n)
    print("---------------------------")
    print(A6)    
    print("---------------------------")
    print(b6)
    print("---------------------------")
    

    A = np.vstack((A1, A2, A3, A4, A5, A6))
    b = np.hstack((b1, b2, b3, b4, b5, b6))
    print("---------------------------")
    print(A)    
    print("---------------------------")
    print(b)
    print("---------------------------")    
    

    res = linprog(c, A_ub=A, b_ub=b, options={"disp": True})
    X = res.x

    wL = np.zeros(n)
    wU = np.zeros(n)
    for i in range(n):
        wL[i] = X[i]
        wU[i] = X[i+n]


    print("X = ")
    print(res.x)
    print("J* = ")
    print(res.fun)
    print("wL = ", end=" ")
    print(wL)
    print("wU = ", end=" ")
    print(wU)
    
    return {"J*":res.fun, "wL":wL, "wU":wU}






def additivModel(L,U):

    if len(L) != len(U):
        errorCode = 0
        print("Error")
        
    n = len(L)

    c = np.zeros(2*n+2*n*(n-1))
    for i in range(2*n, 2*n+2*n*(n-1)):
        c[i] = 1
##    print(c)

    
    A1_1_eq = np.zeros((n*(n-1)/2, n))
    k = 0
    for i in range(n):
        for j in range(n):
            if i<j:
                A1_1_eq[k][i] = 1 - L[i][j]
                k += 1

    A1_2_eq = np.zeros((n*(n-1)/2, n))
    k = 0
    for i in range(n):
        for j in range(n):
            if i<j:
                A1_2_eq[k][j] = - L[i][j]
                k += 1
        
    A1_3_eq = np.eye(n*(n-1)/2)
    A1_4_eq = -np.eye(n*(n-1)/2)
    A1_5_eq = np.zeros((n*(n-1)/2, n*(n-1)/2))
    A1_6_eq = np.zeros((n*(n-1)/2, n*(n-1)/2))


    A1_eq = np.hstack( (A1_1_eq, A1_2_eq, A1_3_eq, A1_4_eq, A1_5_eq, A1_6_eq) )
    b1_eq = np.zeros(n*(n-1)/2)
    
##    print("---------------------------")
##    print(A1_1_eq)
##    print("---------------------------")
##    print(A1_2_eq)
##    print("---------------------------")
##    print(A1_3_eq)
##    print("---------------------------")
##    print(A1_4_eq)
##    print("---------------------------")
##    print(A1_5_eq)
##    print("---------------------------")
##    print(A1_6_eq)
##    print("---------------------------")
##    print(A1_eq)    
##    print("---------------------------")
##    print(b1_eq)
##    print("---------------------------")


    A2_1_eq = np.zeros((n*(n-1)/2, n))
    k = 0
    for i in range(n):
        for j in range(n):
            if i<j:
                A2_1_eq[k][j] = - U[i][j]
                k += 1
                
    A2_2_eq = np.zeros((n*(n-1)/2, n))
    k = 0
    for i in range(n):
        for j in range(n):
            if i<j:
                A2_2_eq[k][i] = 1 - U[i][j]
                k += 1

    A2_3_eq = np.zeros((n*(n-1)/2, n*(n-1)/2))
    A2_4_eq = np.zeros((n*(n-1)/2, n*(n-1)/2))
    A2_5_eq = np.eye(n*(n-1)/2)
    A2_6_eq = -np.eye(n*(n-1)/2)


    A2_eq = np.hstack( (A2_1_eq, A2_2_eq, A2_3_eq, A2_4_eq, A2_5_eq, A2_6_eq) )
    b2_eq = np.zeros(n*(n-1)/2)

##    print("---------------------------")
##    print(A2_eq)    
##    print("---------------------------")
##    print(b2_eq)
    
    Aeq = np.vstack( (A1_eq, A2_eq) )
    beq = np.zeros(n*(n-1)) 

##    print("---------------------------")
##    print(Aeq)    
##    print("---------------------------")
##    print(beq)
##    print("---------------------------")

    A3 = np.hstack( (np.eye(n), -np.eye(n), np.zeros((n, 2*n*(n-1)))) )
    b3 = np.zeros(n)

##    print("---------------------------")
##    print(A3)    
##    print("---------------------------")
##    print(b3)
##    print("---------------------------")


    A4_1_1 = -np.eye(n)
    A4_1_2 = -( np.ones((n,n)) - np.eye(n) )   
    A4_1 = np.hstack( (A4_1_1, A4_1_2, np.zeros((n, 2*n*(n-1)))))
    b4_1 = - np.ones(n)
            
##    print("---------------------------")
##    print(A4_1)    
##    print("---------------------------")
##    print(b4_1)
##    print("---------------------------")
    
    
    A4_2_1 = np.ones((n,n)) - np.eye(n)
    A4_2_2 = np.eye(n)
    A4_2 =  np.hstack( (A4_2_1, A4_2_2, np.zeros((n, 2*n*(n-1)))))    
    b4_2 = np.ones(n)
            
##    print("---------------------------")
##    print(A4_2)    
##    print("---------------------------")
##    print(b4_2)
##    print("---------------------------")

    
    A4 = np.vstack((A4_1, A4_2))
    b4 = np.hstack((b4_1, b4_2))

##    print("---------------------------")
##    print(A4)    
##    print("---------------------------")
##    print(b4)
##    print("---------------------------")


    A = np.vstack((A3, A4))
    b = np.hstack((b3, b4))
   
##    print("---------------------------")
##    print(A)    
##    print("---------------------------")
##    print(b)
##    print("---------------------------")

    
    
    res = linprog(c, A_ub=A, b_ub=b, A_eq=Aeq, b_eq=beq, options={"disp": True})
    X = res.x

    wL = np.zeros(n)
    wU = np.zeros(n)
    for i in range(n):
        wL[i] = X[i]
        wU[i] = X[i+n]


    print("X = ")
    print(res.x)
    print("J* = ")
    print(res.fun)
    print("wL = ", end=" ")
    print(wL)
    print("wU = ", end=" ")
    print(wU)
    
    return {"J*":res.fun, "wL":wL, "wU":wU}





##def TLGPmodel_LP(L,U):
##    # Это нерабочая модель
##
##    if len(L) != len(U):
##        errorCode = 0
##        print("Error")
##        
##    n = len(L)
##
##    c = np.hstack( (np.zeros(2*n), np.ones(n*(n-1))) )
##    print(c)
##
##
##    Aeq = np.hstack( (np.ones(n), -np.ones(n), np.zeros(n*(n-1))) ) 
##    beq = 0
##    Aeq = np.vstack( (Aeq, np.zeros(2*n+n*(n-1))) )
##    beq = np.hstack((beq,0))
##    print("-------------------------")
##    print(Aeq)
##    print("-------------------------")
##    print(beq)
##    print("-------------------------")
##
##    A1_1 = np.zeros( (n*(n-1)/2, n) )
##    A1_2 = np.zeros( (n*(n-1)/2, n) )
##    b1 = np.zeros(n*(n-1)/2)
##    k = 0
##    for i in range(n):
##        for j in range(n):
##            if i<j:
##                A1_1[k][i] = 1
##                A1_2[k][i] = -1
##                A1_1[k][j] = -1
##                A1_2[k][j] = 1
##                b1[k] = np.log(U[i][j])
##                k += 1
##    A1_3 = np.zeros( (n*(n-1)/2, n*(n-1)/2) )
##    A1_4 = - np.eye(n*(n-1)/2)
##
##    
##    A1 = np.hstack((A1_1, A1_2, A1_3, A1_4))
##    print("-------------------------")
##    print(A1)
##    print("-------------------------")
##    print(b1)
##    print("-------------------------")
##
##    A2_1 = - A1_1
##    A2_2 = - A1_2
##    A2_3 = - np.eye(n*(n-1)/2)
##    A2_4 = np.zeros( (n*(n-1)/2, n*(n-1)/2) )
##
##    b2 = np.zeros(n*(n-1)/2)
##    k = 0
##    for i in range(n):
##        for j in range(n):
##            if i<j:
##                b2[k] = -np.log(L[i][j])
##                k += 1
##
##    A2 = np.hstack((A2_1, A2_2, A2_3, A2_4))
##    print("-------------------------")
##    print(A2)
##    print("-------------------------")
##    print(b2)
##    print("-------------------------")
##
##    A = np.vstack((A1, A2))
##    b = np.hstack((b1, b2))
##   
##    print("---------------------------")
##    print(A)    
##    print("---------------------------")
##    print(b)
##    print("---------------------------")
##
##
##    res = linprog(c, A_ub=A, b_ub=b, A_eq=Aeq, b_eq=beq, options={"disp": True})
##    X = res.x
##
##    wL = np.zeros(n)
##    wU = np.zeros(n)
##    for i in range(n):
##        wL[i] = X[i]
##        wU[i] = X[i+n]
##
##
##    print("X = ")
##    print(res.x)
##    print("J* = ")
##    print(res.fun)
##    print("wL = ", end=" ")
##    print(wL)
##    print("wU = ", end=" ")
##    print(wU)
##    
##    return {"J*":res.fun, "wL":wL, "wU":wU}
##
