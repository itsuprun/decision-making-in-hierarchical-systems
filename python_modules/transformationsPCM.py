# преобразования МПС из мультипликативной в аддитивную и наоборот
# преобразования МПС из четкой в интервальную и наоборот
# преобразования МПС из четкой в треугольную нечеткую и наоборот

import numpy as np
import intervalArithmetic as IA 


def getAdditivPCM(D): # D - мультипликативная четкая МПС
    # возвращает аддитивную четкую МПС

    n = len(D)
    
    B = np.zeros((n, n))
    for i in range(n):
        for j in range(n):
            B[i][j] = D[i][j] / (1+D[i][j])

    return B



def getMultPCM(B): # B - аддитивная четкая МПС
    # возвращает мультипликативную четкую МПС

    n = len(B)
    
    D = np.zeros((n, n))
    for i in range(n):
        for j in range(n):
            D[i][j] = B[i][j] / B[j][i]

    return D 



def getAdditivIntervalPCM(L,U): # D=[L,U] - мультипликативная интервальная МПС
    # возвращает аддитивную интервальную МПС B=[BL, BU]

    if len(L) != len(U):
        errorCode = 0
        print("Error")

    n = len(L)
    
    BL = np.zeros((n, n))
    BU = np.zeros((n, n))
    for i in range(n):
        for j in range(n):
            res = IA.division( L[i][j], U[i][j], IA.summ(1,1,L[i][j],U[i][j])["L"], IA.summ(1,1,L[i][j],U[i][j])["U"])
            BL[i][j] = res["L"]
            BU[i][j] = res["U"]

    return {"L": BL, "U": BU}
