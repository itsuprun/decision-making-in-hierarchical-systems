import findLocalWsIPCM as LP
import transformationsPCM as TRANSFORM


# Test 1
##L = [[0.5, 0.35, 0.5, 0.45], [0.5, 0.5, 0.55, 0.5], [0.4, 0.3, 0.5, 0.4], [0.4, 0.3, 0.45, 0.50]]
##U = [[0.5, 0.5, 0.6, 0.6], [0.65, 0.5, 0.7, 0.7], [0.5, 0.45, 0.5, 0.55], [0.55, 0.5, 0.6, 0.5]]
##print( LP.additivModel(L,U) )
##Successful


### Test 2
##L = [[0.5, 0.35, 0.5], [0.5, 0.5, 0.55], [0.4, 0.3, 0.5]]
##U = [[0.5, 0.5, 0.6], [0.65, 0.5, 0.7], [0.5, 0.45, 0.5]]
##print( LP.additivModel(L,U) )


### Test 3 (нечеткая слабо несогласованная МПС 5x5)
L = [[1, 1, 4, 3, 2], [1/5, 1, 1/6, 1/7, 3], [1/8, 2, 1, 1/4, 1/7], [1/7, 3, 1, 1, 1], [1/6,1/7,3,1/5,1]]
U = [[1, 5, 8, 7, 6], [  1, 1, 1/2, 1/3, 7], [1/4, 6, 1,   1, 1/3], [1/3, 7, 4, 1, 5], [1/2,1/3,7,  1,1]]
B = TRANSFORM.getAdditivIntervalPCM(L,U)
print(B)
print(B["L"])
print(B["U"])
print( LP.additivModel(B["L"], B["U"]) )
### результат: J* = 2.10651265614
###wL =  [ 0.06238599  0.03830719  0.03283473  0.05472455  0.        ]
###wU =  [ 0.06238599  0.31192995  0.3064575   0.31922656  0.27362277]



# Test 4 (четкая слабо несогласованная МПС_4 5x5)
##D = [[[1,1], [1,1], [2,2], [4,4], [1/2,1/2]], [[1,1], [1,1], [2,2], [4,4], [8,8]], [[1/2,1/2],[1/2,1/2],[1,1],[2,2], [4,4]], [[1/4,1/4],[1/4,1/4],[1/2,1/2],[1,1],[2,2]], [[2,2],[1/8,1/8],[1/4,1/4],[1/2,1/2],[1,1]]]
##L = [[1,1,2,4,1/2], [1,1,2,4,8], [1/2,1/2,1,2,4], [1/4,1/4,1/2,1,2], [2,1/8,1/4,1/2,1]]
##U = [[1,1,2,4,1/2], [1,1,2,4,8], [1/2,1/2,1,2,4], [1/4,1/4,1/2,1,2], [2,1/8,1/4,1/2,1]]
##B = TRANSFORM.getAdditivIntervalPCM(L,U)
##print(B)
##print(B["L"])
##print(B["U"])
##print( LP.additivModel(B["L"], B["U"]) )
###результат: J* = 0.434782608696
###wL =  [ 0.34782609  0.34782609  0.17391304  0.08695652  0.04347826]
###wU =  [ 0.34782609  0.34782609  0.17391304  0.08695652  0.04347826]



# Test А1_1 (четкая полностью согласованная МПС 5x5)
##D = [[[1,1], [1,1], [2,2], [4,4], [8,8]], [[1,1], [1,1], [2,2], [4,4], [8,8]], [[1/2,1/2],[1/2,1/2],[1,1],[2,2], [4,4]], [[1/4,1/4],[1/4,1/4],[1/2,1/2],[1,1],[2,2]], [[2,2],[1/8,1/8],[1/4,1/4],[1/2,1/2],[1,1]]]
##L = [[1,1,2,4,8], [1,1,2,4,8], [1/2,1/2,1,2,4], [1/4,1/4,1/2,1,2], [1/8,1/8,1/4,1/2,1]]
##U = [[1,1,2,4,8], [1,1,2,4,8], [1/2,1/2,1,2,4], [1/4,1/4,1/2,1,2], [1/8,1/8,1/4,1/2,1]]
##print( LP.LowerLUAMmodel(L,U) )
##J* = -0.0
##wL =  [ 0.34782609  0.34782609  0.17391304  0.08695652  0.04347826]
##wU =  [ 0.34782609  0.34782609  0.17391304  0.08695652  0.04347826]
#Success



# Test А3_1 (четкая слабо несогласованная МПС_4 5x5)
##D = [[[1,1], [1,1], [2,2], [4,4], [1/2,1/2]], [[1,1], [1,1], [2,2], [4,4], [8,8]], [[1/2,1/2],[1/2,1/2],[1,1],[2,2], [4,4]], [[1/4,1/4],[1/4,1/4],[1/2,1/2],[1,1],[2,2]], [[2,2],[1/8,1/8],[1/4,1/4],[1/2,1/2],[1,1]]]
##L = [[1,1,2,4,1/2], [1,1,2,4,8], [1/2,1/2,1,2,4], [1/4,1/4,1/2,1,2], [2,1/8,1/4,1/2,1]]
##U = [[1,1,2,4,1/2], [1,1,2,4,8], [1/2,1/2,1,2,4], [1/4,1/4,1/2,1,2], [2,1/8,1/4,1/2,1]]
##print( LP.UpperLUAMmodel(L,U) )
##J* = 0.434782608696
##wL =  [ 0.13043478  0.34782609  0.17391304  0.08695652  0.04347826]
##wU =  [ 0.34782609  0.34782609  0.17391304  0.08695652  0.26086957]
# Success


# Test А3_2 (четкая слабо несогласованная МПС_4 5x5)
##D = [[[1,1], [1,1], [2,2], [4,4], [1/2,1/2]], [[1,1], [1,1], [2,2], [4,4], [8,8]], [[1/2,1/2],[1/2,1/2],[1,1],[2,2], [4,4]], [[1/4,1/4],[1/4,1/4],[1/2,1/2],[1,1],[2,2]], [[2,2],[1/8,1/8],[1/4,1/4],[1/2,1/2],[1,1]]]
##L = [[1,1,2,4,1/2], [1,1,2,4,8], [1/2,1/2,1,2,4], [1/4,1/4,1/2,1,2], [2,1/8,1/4,1/2,1]]
##U = [[1,1,2,4,1/2], [1,1,2,4,8], [1/2,1/2,1,2,4], [1/4,1/4,1/2,1,2], [2,1/8,1/4,1/2,1]]
##print( LP.LowerLUAMmodel(L,U) )
#Optimization failed. Unable to find a feasible starting point.
#Модель LowerLUAM не работает и не должна работать на сильно несогласованной ИМПС



#Test A4 (нечеткая слабо согласованная МПС)
#LUAM Upper
##PCM = [[[1,1], [1,3], [3,5], [5,7], [5,9]], [[1/3,1], [1,1], [1,4], [1,5], [1,4]], [[1/5,1/3],[1/4,1],[1,1],[1/5,5], [2,4]], [[1/7,1/5],[1/5,1],[1/5,5],[1,1],[1,2]], [[1/9,1/5],[1/4,1],[1/4,1/2],[1/2,1],[1,1]]]
##L = [[1, 1, 3, 5, 5], [1/3, 1, 1, 1, 1], [1/5,1/4,1,1/5,2], [1/7,1/5,1/5,1,1], [1/9,1/4,1/4,1/2,1] ]
##U = [[1, 3, 5, 7, 9], [  1, 1, 4, 5, 4], [1/3,  1,1,  5,4], [1/5,  1,  5,1,2], [1/5,  1,1/2,  1,1] ]
##print(LP.UpperLUAMmodel(L,U))
##J* = 0.618181818182
##wL =  [ 0.29090909  0.13636364  0.02727273  0.03636364  0.04545455]
##wU =  [ 0.40909091  0.29090909  0.18181818  0.13636364  0.13636364]
## Successful



#Test A4 (нечеткая слабо согласованная МПС)
#LUAM Lower
##PCM = [[[1,1], [1,3], [3,5], [5,7], [5,9]], [[1/3,1], [1,1], [1,4], [1,5], [1,4]], [[1/5,1/3],[1/4,1],[1,1],[1/5,5], [2,4]], [[1/7,1/5],[1/5,1],[1/5,5],[1,1],[1,2]], [[1/9,1/5],[1/4,1],[1/4,1/2],[1/2,1],[1,1]]]
##L = [[1, 1, 3, 5, 5], [1/3, 1, 1, 1, 1], [1/5,1/4,1,1/5,2], [1/7,1/5,1/5,1,1], [1/9,1/4,1/4,1/2,1] ]
##U = [[1, 3, 5, 7, 9], [  1, 1, 4, 5, 4], [1/3,  1,1,  5,4], [1/5,  1,  5,1,2], [1/5,  1,1/2,  1,1] ]
##print(LP.LowerLUAMmodel(L,U))
##J* = -0.223534756929
##wL =  [ 0.42253521  0.17810086  0.14084507  0.07632894  0.07042254]
##wU =  [ 0.53430259  0.28169014  0.14084507  0.08450704  0.07042254]
## Successful



# Test 5 (четкая слабо несогласованная МПС_3 5x5)
##D = [[[1,1], [2,2], [3,3], [5,5], [7,7]], [[1/2,1/2], [1,1], [2,2], [2,2], [4,4]], [[1/3,1/3],[1/2,1/2],[1,1],[1,1], [2,2]], [[1/5,1/5],[1/2,1/2],[1,1],[1,1],[9,9]], [[1/7,1/7],[1/4,1/4],[1/2,1/2],[1/9,1/9],[1,1]]]
##L = [[1, 2, 3, 5, 7], [1/2, 1, 2, 2, 4], [1/3, 1/2, 1, 1, 2], [1/5, 1/2, 1, 1, 9], [1/7, 1/4, 1/2, 1/9, 1]]
##U = [[1, 2, 3, 5, 7], [1/2, 1, 2, 2, 4], [1/3, 1/2, 1, 1, 2], [1/5, 1/2, 1, 1, 9], [1/7, 1/4, 1/2, 1/9, 1]]
##B = TRANSFORM.getAdditivIntervalPCM(L,U)
##print(B)
##print(B["L"])
##print(B["U"])
##print( LP.additivModel(B["L"], B["U"]) )
###результат: J* = 0.195098039216
###wL =  [ 0.47058824  0.23529412  0.11764706  0.11764706  0.05882353]
###wU =  [ 0.47058824  0.23529412  0.11764706  0.11764706  0.05882353]




##Test 5 (четкая слабо несогласованная МПС_3 5x5)
##D = [[[1,1], [2,2], [3,3], [5,5], [7,7]], [[1/2,1/2], [1,1], [2,2], [2,2], [4,4]], [[1/3,1/3],[1/2,1/2],[1,1],[1,1], [2,2]], [[1/5,1/5],[1/2,1/2],[1,1],[1,1],[9,9]], [[1/7,1/7],[1/4,1/4],[1/2,1/2],[1/9,1/9],[1,1]]]
##L = [[1, 2, 3, 5, 7], [1/2, 1, 2, 2, 4], [1/3, 1/2, 1, 1, 2], [1/5, 1/2, 1, 1, 9], [1/7, 1/4, 1/2, 1/9, 1]]
##U = [[1, 2, 3, 5, 7], [1/2, 1, 2, 2, 4], [1/3, 1/2, 1, 1, 2], [1/5, 1/2, 1, 1, 9], [1/7, 1/4, 1/2, 1/9, 1]]
##print(LP.LowerLUAMmodel(L,U))
####Optimization failed. Unable to find a feasible starting point.
#Модель LowerLUAM не работает и не должна работать на сильно несогласованной ИМПС
####print(LP.UpperLUAMmodel(L,U))




#Test 8 (нечеткая слабо согласованная МПС 5x5)
##D = [[[1,1], [1/3,1], [1/4,1/2], [1/7,1/5], [1/3,1]], [[1,3], [1,1], [3,5], [4,6], [1,3]], [[2,4],[1/5,1/3],[1,1],[1/3,1], [1/5,1/3]], [[5,7],[1/6,1/4],[1,3],[1,1],[1/4,1/2]], [[1,3],[1/3,1],[3,5],[2,4],[1,1]]]
##L = [[1, 1/3, 1/4, 1/7, 1/3], [1, 1, 3, 4, 1], [2,1/5,1,1/3,1/5], [5,1/6,1,1,1/4], [1,1/3,3,2,1]]
##U = [[1,   1, 1/2, 1/5,   1], [3, 1, 5, 6, 3], [4,1/3,1,  1,1/3], [7,1/4,3,1,1/2], [3,  1,5,4,1]]
##print(LP.UpperLUAMmodel(L,U))
##print(LP.LowerLUAMmodel(L,U))
####Optimization failed. Unable to find a feasible starting point.
#Модель LowerLUAM не работает и не должна работать на сильно несогласованной ИМПС




#Test 9 (нечеткая согласованная МПС 3x3)
##D = [ [[1,1], [1,3], [3,5]], [[1/3,1], [1,1], [1,3]], [[1/5,1/3],[1/3,1],[1,1]] ]
##L = [ [1, 1, 3], [1/3, 1, 1], [1/5, 1/3, 1] ]
##U = [ [1, 3, 5], [  1, 1, 3], [1/3,   1, 1] ]
##print(LP.UpperLUAMmodel(L,U))
##print(LP.LowerLUAMmodel(L,U))
####Модель LowerLUAM Successful !!!!!!!!!!!!!!!!!!!!!!!!!!



#Test A5 (interval PCM, weak inconsistent)
##L = [ [1, 1/3, 1/6, 1/8, 1/9], [1, 1, 1/3, 1/4, 1/5], [4, 1, 1, 1, 1/3], [6, 2, 1/3, 1, 1], [8, 3, 1, 1, 1] ]
##U = [ [1,   1, 1/4, 1/6, 1/8], [3, 1,   1, 1/2, 1/3], [6, 3, 1, 3,   1], [8, 4,   1, 1, 1], [9, 5, 3, 1, 1] ]
####print(LP.UpperLUAMmodel(L,U))
####J* = 0.542635658915
####wL =  [ 0.03875969  0.06976744  0.11627907  0.11627907  0.34883721]
####wU =  [ 0.06976744  0.11627907  0.34883721  0.34883721  0.34883721]
##print(LP.LowerLUAMmodel(L,U))
###Optimization failed. Unable to find a feasible starting point.
#Модель LowerLUAM не работает и не должна работать на сильно несогласованной ИМПС



#Test corrected A5 (interval PCM, weak consistent)
##L = [ [1, 1/3, 1/6, 1/8, 1/9], [1, 1, 1/3, 1/4, 1/5], [4, 1, 1, 1/3, 1/3], [6, 2, 1, 1, 1], [8, 3, 1, 1, 1] ]
##U = [ [1,   1, 1/4, 1/6, 1/8], [3, 1,   1, 1/2, 1/3], [6, 3, 1,   1,   1], [8, 4, 3, 1, 1], [9, 5, 3, 1, 1] ]
####print(LP.UpperLUAMmodel(L,U))
######J* = 0.373134328358
######wL =  [ 0.03731343  0.06716418  0.1119403   0.29850746  0.29850746]
######wU =  [ 0.06716418  0.14925373  0.29850746  0.3358209   0.3358209 ]
##print(LP.LowerLUAMmodel(L,U))
####J* = -0.056338028169
####wL =  [ 0.04225352  0.08450704  0.16901408  0.33802817  0.33802817]
####wU =  [ 0.04225352  0.11267606  0.1971831   0.33802817  0.33802817]
####Модель LowerLUAM Successful !!!!!!!!!!!!!!!!!!!!!!!!!!


#Test A6 (interval PCM, weak inconsistent)
##L = [ [1, 1/7, 1, 1/7, 1/9], [5, 1, 2, 1/3, 1], [1, 1/4, 1, 1/9, 1/5], [5, 1, 8, 1, 1/3], [7, 1/3, 3, 1, 1] ]
##U = [ [1, 1/5, 1, 1/5, 1/7], [7, 1, 4,   1, 3], [1, 1/2, 1, 1/8, 1/3], [7, 3, 9, 1,   1], [9,   1, 5, 3, 1] ]
###print(LP.UpperLUAMmodel(L,U))
####J* = 0.787234042553
####wL =  [ 0.04255319  0.12765957  0.04255319  0.12765957  0.12765957]
####wU =  [ 0.04255319  0.38297872  0.06382979  0.38297872  0.38297872]
##print(LP.LowerLUAMmodel(L,U))
###Optimization failed. Unable to find a feasible starting point.
#Модель LowerLUAM не работает и не должна работать на сильно несогласованной ИМПС



#Test corrected A6 (interval PCM, weak consistent)
##L = [ [1, 1/7, 1, 1/7, 1/9], [5, 1, 2, 1/3, 1/3], [1, 1/4, 1, 1/9, 1/5], [5, 1, 8, 1, 1/3], [7, 1, 3, 1, 1] ]
##U = [ [1, 1/5, 1, 1/5, 1/7], [7, 1, 4,   1,   1], [1, 1/2, 1, 1/8, 1/3], [7, 3, 9, 1,   1], [9, 3, 5, 3, 1] ]
###print(LP.UpperLUAMmodel(L,U))
####J* = 0.559701492537
####wL =  [ 0.04477612  0.1119403   0.03731343  0.13432836  0.31343284]
####wU =  [ 0.04477612  0.31343284  0.10447761  0.3358209   0.40298507]
##print(LP.LowerLUAMmodel(L,U))
###Optimization failed. Unable to find a feasible starting point.
#Модель LowerLUAM не работает и не должна работать на сильно несогласованной ИМПС


#Test A7 (interval PCM, weak inconsistent)
##L = [ [1, 1, 2, 4, 6], [1/3, 1, 1, 1/3, 3], [1/4, 1/3, 1, 1, 1], [1/6, 1, 1/3, 1, 8], [1/8, 1/5, 1/3, 1/10, 1] ]
##U = [ [1, 3, 4, 6, 8], [  1, 1, 3,   1, 5], [1/2,   1, 1, 3, 3], [1/4, 3,   1, 1,10], [1/6, 1/3,   1, 1/8,  1] ]
####print(LP.UpperLUAMmodel(L,U))
####J* = 0.627142857143
####wL =  [ 0.34285714  0.1         0.08571429  0.05714286  0.03      ]
####wU =  [ 0.34285714  0.34285714  0.17142857  0.3         0.08571429]
##print(LP.LowerLUAMmodel(L,U))
###Optimization failed. Unable to find a feasible starting point.
#Модель LowerLUAM не работает и не должна работать на сильно несогласованной ИМПС



#Test corrected A7 (interval PCM, weak inconsistent)
##L = [ [1, 1, 2, 4, 6], [1/3, 1, 1, 1, 3], [1/4, 1/3, 1, 1, 1], [1/6, 1/3, 1/3, 1, 8], [1/8, 1/5, 1/3, 1/10, 1] ]
##U = [ [1, 3, 4, 6, 8], [  1, 1, 3, 3, 5], [1/2,   1, 1, 3, 3], [1/4,   1,   1, 1,10], [1/6, 1/3,   1, 1/8,  1] ]
###print(LP.UpperLUAMmodel(L,U))
####J* = 0.530909090909
####wL =  [ 0.32727273  0.14545455  0.10909091  0.05454545  0.01454545]
####wU =  [ 0.43636364  0.32727273  0.16363636  0.14545455  0.10909091]
##print(LP.LowerLUAMmodel(L,U))
###Optimization failed. Unable to find a feasible starting point.
#Модель LowerLUAM не работает и не должна работать на сильно несогласованной ИМПС
