from numpy import linalg as LA
from scipy.optimize import linprog
import numpy as np
import findConsistencyIndexesPCM as CONS_IND

def initialization(n):

    w = list()
    if n>0:
        for i in range(n):
            w.append(1/n)

    return w

 
def EigenvectorMethod(PCM):
    N = len(PCM)

    w = list()
    for i in range(N):
        w.append(1/N)

    if N>1:
        lamda, eigvect = LA.eig(PCM)
#        print(lamda)
#        print(eigvect)

        lamdaMax = max(lamda).real
#        print(lamdaMax)

        i_lamdaMax = 0
        for i in range(len(lamda)):
            if lamda[i] == max(lamda):
                i_lamdaMax = i

        w = list(eigvect[:,i_lamdaMax])

        for i in range(N):
            w[i] = w[i].real
            
#        print(w)
            
    return w


def RowGeometricMeanMethod(PCM):
    N = len(PCM)

    w = list()
    for i in range(N):
        w.append(1/N)

    if N>1:
        for i in range(N):
            prod=1
            for j in range(N):
                prod = prod*PCM[i][j]
            w[i] = pow(prod,1/N)
            
    return w


def ArithmeticNormalizationMethod(PCM):
    N = len(PCM)

    w = list()
    for i in range(N):
        w.append(1/N)
        
    if N>1:
        for j in range(N):
            s=0
            for i in range(N):
                s = s + PCM[i][j]
            w[j] = pow(s,-1)
            
    return w


def findMatrixProportions(PCM):
    N = len(PCM)
    Z = list() # Матрица NxN

    for i in range(N):
        Z.append([])
        for j in range(N):
            Z[i].append(PCM[i][j] / (1+PCM[i][j]) )   

    return Z


def sumElemsList(x):
    s = 0
    N = len(x)
    for i in range(N):
        s += x[i]
        
    return s


def StableUFOsMethod(PCM):
    N = len(PCM)
    Z = list() # Матрица NxN
    C = list() # Вектор 1хN
    Diag = list() # Диагональная матрица NxN
    
    w = list()
    for i in range(N):
        w.append(1/N)
          
    if N>1:                            
        Z = findMatrixProportions(PCM)

        for i in range(N):
            Diag.append([])
            for j in range(N):
                Diag[i].append(0)

        for i in range(N):
            C.append(sumElemsList(Z[i]))

        for i in range(N):
            Diag[i][i] = C[i]

        for i in range(N):
            for j in range(N):
                Z[i][j] = Z[i][j] + Diag[i][j]

        w = EigenvectorMethod(Z)               

##        print(Diag)
##        print("------------------------------")
##        print(Z)
##        print("------------------------------")
##        print(w)

    return w




def CrispLUAMmodel(D):

    n = len(D)

    c = np.hstack( (-np.ones(n), np.ones(n)) )
#    print(c)

    A1_1 = np.zeros((n*n, n))
    k = 0
    for i in range(n):
        for j in range(n):
#            if i!=j:
            A1_1[k][i] = 1
            k += 1
    A1_2 = np.zeros((n*n, n))
    k = 0
    for i in range(n):
        for j in range(n):
##            if i!=j:
            A1_2[k][j] = - D[i][j]
            k += 1

    A1 = np.hstack( (A1_1, A1_2 ))                
    b1 = np.zeros(n*n)           
##    print("---------------------------")
##    print(A1)    
##    print("---------------------------")
##    print(b1)
##    print("---------------------------")


    A2_1 = np.zeros((n*n, n))
    k = 0
    for i in range(n):
        for j in range(n):
##            if j!=i:
            A2_1[k][i] = D[j][i]
            k += 1
    A2_2 = np.zeros((n*n, n))
    k = 0
    for i in range(n):
        for j in range(n):
##            if j!=i:
            A2_2[k][j] = -1
            k += 1

    A2 = np.hstack( (A2_1, A2_2 ))                
    b2 = np.zeros(n*n)           
##    print("---------------------------")
##    print(A2)    
##    print("---------------------------")
##    print(b2)
##    print("---------------------------")               


    A3_1 = -np.eye(n)
    A3_2 = -( np.ones((n,n)) - np.eye(n) )   
    A3 = np.hstack( (A3_1, A3_2 ))
    b3 = - np.ones(n)
            
##    print("---------------------------")
##    print(A3)    
##    print("---------------------------")
##    print(b3)
##    print("---------------------------")
    
    
    A4_1 = np.ones((n,n)) - np.eye(n)
    A4_2 = np.eye(n)
    A4 =  np.hstack( (A4_1, A4_2 ))    
    b4 = np.ones(n)
            
##    print("---------------------------")
##    print(A4)    
##    print("---------------------------")
##    print(b4)
##    print("---------------------------")


    A5 = np.hstack( (np.eye(n), -np.eye(n)) )
    b5 = np.zeros(n)
##    print("---------------------------")
##    print(A5)    
##    print("---------------------------")
##    print(b5)
##    print("---------------------------")

    eps = 0.001
    A6 = np.hstack( (-np.eye(n), np.zeros((n,n))) )
    b6 = - eps*np.ones(n)
##    print("---------------------------")
##    print(A6)    
##    print("---------------------------")
##    print(b6)
##    print("---------------------------")
    

    A = np.vstack((A1, A2, A3, A4, A5, A6))
    b = np.hstack((b1, b2, b3, b4, b5, b6))
##    print("---------------------------")
##    print(A)    
##    print("---------------------------")
##    print(b)
##    print("---------------------------")    
    

    res = linprog(c, A_ub=A, b_ub=b, options={"disp": True})
    X = res.x

    wL = np.zeros(n)
    wU = np.zeros(n)
    for i in range(n):
        wL[i] = X[i]
        wU[i] = X[i+n]


    print("X = ")
    print(res.x)
    print("J* = ")
    print(res.fun)
    print("wL = ", end=" ")
    print(wL)
    print("wU = ", end=" ")
    print(wU)
    
    return {"J*":res.fun, "wL":wL, "wU":wU}





def normalizationWs(w):
    N = len(w)

    normW = w
    
    if N>1:
        normW = list()
        s = 0
        for i in range(N):
            s += w[i]
        for i in range(N):
            normW.append(w[i]/s)

    return normW




def ChebNorm(wL,wU):

    if len(wL) != len(wU):
        errorCode = 0
        print("Error")
        
    max = 0
    n = len(wL)
    for i in range(n):
        if abs(wU[i] - wL[i]) > max:
            max = wU[i] - wL[i]

    return max
    



def EuclNorm(wL,wU):

    if len(wL) != len(wU):
        errorCode = 0
        print("Error")
        
    s = 0
    n = len(wL)
    for i in range(n):
        s = s + pow(wU[i] - wL[i], 2)

    return pow(s, 1/2)
