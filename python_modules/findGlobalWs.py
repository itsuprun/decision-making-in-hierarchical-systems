import copy

# find global (aggregated) weights of a set of alternatives in terms of multiple criteria

def initialization(N): # n - number of alternatives

    alternGlobWs = list()

    if N>0:
        for i in range(N):
            alternGlobWs.append(1/N)

    return alternGlobWs



def DistributiveMethod(alternLocWs, critGlobWs):

    N = len(alternLocWs) # number of alternatives
    M = len(critGlobWs)  # number of criteria

    alternGlobWs = list()
    for i in range(N):
        alternGlobWs.append(1/N)

    if (sum(critGlobWs) > 0.999) and (sum(critGlobWs) < 1.001):
        errorCode=2 # Вектор весов не нормирован (сумма не равна 1)


    if len(alternLocWs[0]) == len(critGlobWs):
    
        s = list()
        for j in range(M):
            s.append(0)

        # normalization of alternLocWs in terms of criteria
        for j in range(M):
            s[j]=0
            for i in range(N):
                s[j] += alternLocWs[i][j]

        # aggregation
        for i in range(N): # i-th alternative
            s2=0               
            for j in range(M): # j-th criterion
                s2 += (alternLocWs[i][j] / s[j]) * critGlobWs[j]

            alternGlobWs[i] = s2

        errorCode=1
            
    else:
        errorCode=0  # Размерности векторов не совпадают
        
    return {"GlobWs":alternGlobWs, "errorCode":errorCode}



def IdealMethod(alternLocWs, critGlobWs):

    N = len(alternLocWs) # number of alternatives
    M = len(critGlobWs)  # number of criteria

    alternGlobWs = list()
    for i in range(N):
        alternGlobWs.append(1/N)

        
    if len(alternLocWs[0]) == len(critGlobWs):
        if (sum(critGlobWs) > 0.999) and (sum(critGlobWs) < 1.001):

            maxW = list()
            for j in range(M):
                maxW.append(0)

            # normalization of alternLocWs in terms of criteria
            for j in range(M):
                maxW[j] = alternLocWs[0][j]
                for i in range(N):
                    if alternLocWs[i][j] > maxW[j]:
                        maxW[j] = alternLocWs[i][j]

            # aggregation
            for i in range(N): # i-th alternative
                s2=0               
                for j in range(M): # j-th criterion
                    s2 += (alternLocWs[i][j] / maxW[j]) * critGlobWs[j]

                alternGlobWs[i] = s2

            errorCode=1
            
        else:
            errorCode=2
    else:
        errorCode=0
        
    return {"GlobWs":alternGlobWs, "errorCode":errorCode}




def MultiplicativeMethod(alternLocWs, critGlobWs):

    N = len(alternLocWs) # number of alternatives
    M = len(critGlobWs)  # number of criteria

    alternGlobWs = list()
    for i in range(N):
        alternGlobWs.append(1/N)
        

    if len(alternLocWs[0]) == len(critGlobWs):
        if (sum(critGlobWs) > 0.999) and (sum(critGlobWs) < 1.001):

            # aggregation
            for i in range(N): # i-th alternative
                prod = 1               
                for j in range(M): # j-th criterion
                    prod *= pow(alternLocWs[i][j], critGlobWs[j])

                alternGlobWs[i] = prod

            errorCode=1
            
        else:
            errorCode=2
    else:
        errorCode=0
        
    return {"GlobWs":alternGlobWs, "errorCode":errorCode}





def MaxMinMethod(alternLocWs, critGlobWs):

    N = len(alternLocWs) # number of alternatives
    M = len(critGlobWs)  # number of criteria

    alternGlobWs = list()
    for i in range(N):
        alternGlobWs.append(1/N)
        

    if len(alternLocWs[0]) == len(critGlobWs):
        if (sum(critGlobWs) > 0.999) and (sum(critGlobWs) < 1.001):

            # aggregation
            for i in range(N): # i-th alternative
                minProd = alternLocWs[i][0] * critGlobWs[0]               
                for j in range(M): # j-th criterion
                    if alternLocWs[i][j] * critGlobWs[j] < minProd:
                        minProd = alternLocWs[i][j] * critGlobWs[j]  

                alternGlobWs[i] = minProd

            errorCode=1
            
        else:
            errorCode=2
    else:
        errorCode=0
        
    return {"GlobWs":alternGlobWs, "errorCode":errorCode}




def GroupBinaryPrefMethod(alternLocWs, critGlobWs):

    N = len(alternLocWs) # number of alternatives
    M = len(critGlobWs)  # number of criteria

    alternGlobWs = list()
    for i in range(N):
        alternGlobWs.append(1/N)

        

    return glob_w


def SugenoMeasure(critWs, lamda):  # построение меры Сугено по правилу g(AUB) = g(A)+g(B)+lamda*g(A)*g(B),  lamda > -1
                            # lamda - параметр нормировки
    N = len(critWs)         # critWs - вектор весов критериев,  g({Ci}) = critWs
    ro = set()   # ro - одно из множеств {C1},{C2},...,{CN}, {C1,C2},...,{C1,CN},...,{C[N-1],C[N]}, {C1,C2,C3},...,{C1,C2,...,CN} 
    g = float()  # g - значение меры соответствующего эл-та ro (множества критериев)
    roList = list() # список всех подмножеств множества критериев
    gList = list()   # каждый эл-т списка g соответствует своему элементу списка roList
                 # мера строится по правилу g({Ci,Cj}) = g({Ci}U{Cj}) = g({Ci}) + g({Cj}) + lamda * g({Ci}) * g({Cj}),  lamda > -1

    roList.append([])
    gList.append([])
    
    for i in range(len(critWs)):
        ro = set()
        ro.add(i) # построили одноэл.множество {Ci}        
        roList[0].append(ro) # построили список одноэл.множеств [{C0}, {C1},..., {C[N-1]} ]
        gList[0].append(critWs[i]) # мера каждого одноэлементного множества из списка roList

    k = 0
    empty = set()
    
    while len(roList[k][0]) < len(critWs):
        roList_2 = list()
        gList_2 = list()
        for i in range(len(roList[k])):
            for j in range(len(roList[0])):

                if roList[k][i] & roList[0][j] == empty:
                    ro = set(roList[k][i] | roList[0][j])
                    g = gList[k][i] + gList[0][j] + lamda * gList[k][i] * gList[0][j]

                if (not (ro in roList_2) ) and (len(ro) == k+2) :
                    roList_2.append(ro)
                    gList_2.append(g)
##        print(roList_2)
        roList.append(roList_2)
        gList.append(gList_2)
##        print(roList)
##        print(gList)
        k +=1

    
    return {"roList": roList, "gList": gList}



def TsukamotoMeasure(critWs, v):  # построение меры Цукамото по правилу g(AUB) = (1-v) * max(g(A),g(B)) + v * (g(A)+g(B)),  v >= 0
                            # v - параметр нормировки
    N = len(critWs)         # critWs - вектор весов критериев,  g({Ci}) = critWs
    ro = set()   # ro - одно из множеств {C1},{C2},...,{CN}, {C1,C2},...,{C1,CN},...,{C[N-1],C[N]}, {C1,C2,C3},...,{C1,C2,...,CN} 
    g = float()  # g - значение меры соответствующего эл-та ro (множества критериев)
    roList = list() # список всех подмножеств множества критериев
    gList = list()   # каждый эл-т списка g соответствует своему элементу списка roList
                 # мера строится по правилу g({Ci,Cj}) = g({Ci}U{Cj}) = (1-v) * max(g({Ci}), g({Cj})) + v * (g({Ci}) + g({Cj})),  v >= 0

    roList.append([])
    gList.append([])
    
    for i in range(len(critWs)):
        ro = set()
        ro.add(i) # построили одноэл.множество {Ci}        
        roList[0].append(ro) # построили список одноэл.множеств [{C0}, {C1},..., {C[N-1]} ]
        gList[0].append(critWs[i]) # мера каждого одноэлементного множества из списка roList

    k = 0
    empty = set()
    
    while len(roList[k][0]) < len(critWs):
        roList_2 = list()
        gList_2 = list()
        for i in range(len(roList[k])):
            for j in range(len(roList[0])):

                if roList[k][i] & roList[0][j] == empty:
                    ro = set(roList[k][i] | roList[0][j])
                    g = (1-v) * max(gList[k][i], gList[0][j]) + v * (gList[k][i] + gList[0][j])

                if (not (ro in roList_2) ) and (len(ro) == k+2) :
                    roList_2.append(ro)
                    gList_2.append(g)
##        print(roList_2)
        roList.append(roList_2)
        gList.append(gList_2)
##        print(roList)
##        print(gList)
        k +=1

    
    return {"roList": roList, "gList": gList}




def showMeasure(roList, gList):
    if len(roList) == len(gList):
        for i in range(len(roList)):
            for j in range(len(roList[i])):
                print(roList[i][j], end="  ")
                print(gList[i][j])


def gValue(roList, gList, ro): # возвращает значение меры g(ro) для заданного ro - подмножества критериев из списка roList
    g = 0
    for i in range(len(roList)):
        for j in range(len(roList[i])):
            if roList[i][j] == ro:
                g = gList[i][j]
            
    return g


def findH(alternLocWs, altNumb, critNumb): # alternLocWs - матрица лок.весов альтернатив по критериям
                                           # alt|Numb - номер альтернативы , critNumb - номер критерия                                     

    N = len(alternLocWs)  # number of alternatives
    M = len(alternLocWs[0])  # number of criteria

    H = set()
    if critNumb in range(M):
        for j in range(M):
            if alternLocWs[altNumb][j] >= alternLocWs[altNumb][critNumb]:
                H.add(j)

    return H   # возвращает H = H[critNumb] = ro - множество критериев для заданного критерия critNum и заданной альтернативы altNum 

    
    
def SugenoFuzzyIntegral(alternLocWs, critGlobWs, lamda):  # alternLocWs - матрица лок.весов альтернатив по критериям
                                                          # critGlobWs - вектор глобал.весов критериев

    N = len(alternLocWs) # number of alternatives
    M = len(critGlobWs)  # number of criteria, M = len(alternLocWs[i]) - количество столбцов в матр.alternLocWs

    alternGlobWs = list()
    for i in range(N):
        alternGlobWs.append(1/N)
        
    
    if len(alternLocWs[0]) == len(critGlobWs):
  
        s = list()
        for j in range(M):
            s.append(0)

        # normalization of alternLocWs in terms of criteria
        for j in range(M):
            s[j]=0
            for i in range(N):
                s[j] += alternLocWs[i][j]

        for i in range(N):
            for j in range(M):
                alternLocWs[i][j] = alternLocWs[i][j] / s[j]


        measure = SugenoMeasure(critGlobWs, lamda)
        sugenoIntegrals = list()

        for i in range(N):
            print("Alternative No" + str(i))
            
            minVals = list()
            for j in range(M):
                Hj = findH(alternLocWs, i, j)
                g_Hj = gValue(measure["roList"], measure["gList"], Hj)
                minVals.append( min(alternLocWs[i][j], g_Hj))

                print(Hj)
                print(g_Hj)

            print(minVals)

            sugenoIntegrals.append( max(minVals) )


        for i in range(N):   
            alternGlobWs[i] = sugenoIntegrals[i]

        errorCode=1
            
    else:
        errorCode=0
        
    return {"GlobWs":alternGlobWs, "errorCode":errorCode}





def ShoquetFuzzyIntegral(alternLocWs, critGlobWs, lamda):  # alternLocWs - матрица лок.весов альтернатив по критериям
# отдельные результир.веса могут быть отрицательными !!!    # critGlobWs - вектор глобал.весов критериев

    N = len(alternLocWs) # number of alternatives
    M = len(critGlobWs)  # number of criteria, M = len(alternLocWs[i]) - количество столбцов в матр.alternLocWs

    alternGlobWs = list()
    for i in range(N):
        alternGlobWs.append(1/N)
        
    
    if len(alternLocWs[0]) == len(critGlobWs):
  
        s = list()
        for j in range(M):
            s.append(0)

        # normalization of alternLocWs in terms of criteria
        for j in range(M):
            s[j]=0
            for i in range(N):
                s[j] += alternLocWs[i][j]

        for i in range(N):
            for j in range(M):
                alternLocWs[i][j] = alternLocWs[i][j] / s[j]


        measure = SugenoMeasure(critGlobWs, lamda)
        shoquetIntegrals = list()

        for i in range(N):
            print("Alternative No" + str(i))
            
            s = 0
            for j in range(M):
                Hj = findH(alternLocWs, i, j)
                Hj_1 = findH(alternLocWs, i, j-1)
                g_Hj = gValue(measure["roList"], measure["gList"], Hj)
                g_Hj_1 = gValue(measure["roList"], measure["gList"], Hj_1)

                s += alternLocWs[i][j] * ( g_Hj - g_Hj_1)

                print(Hj)
                print(Hj_1)
                print(g_Hj)
                print(g_Hj_1)

            print(s)

            shoquetIntegrals.append( s )


        for i in range(N):   
            alternGlobWs[i] = shoquetIntegrals[i]

        errorCode=1
            
    else:
        errorCode=0
        
    return {"GlobWs":alternGlobWs, "errorCode":errorCode}
