from numpy import linalg as LA
import findConsistencyIndexesPCM as CONS_INDEX


def isConsist(PCM):
    flag = True
    N = len(PCM)

    if N>2:
        for i in range(N):
            for j in range(N):
                for k in range(N):
                    if PCM[i][j] != PCM[i][k]*PCM[k][j]:
                        flag = False
                        break

    return flag



def isWeakConsist(PCM):
    flag = True
    N = len(PCM)

    if N>2:
        for i in range(N):
            for j in range(i,N):
                for k in range(i,N):
                    if (PCM[i][j] > 1) and (PCM[j][k] > 1) and (PCM[i][k] < 1):
                        flag = False
                        break
                    if (PCM[i][j] == 1) and (PCM[j][k] > 1) and (PCM[i][k] <= 1):
                        flag = False
                        break
                    if (PCM[i][j] > 1) and (PCM[j][k] == 1) and (PCM[i][k] <= 1):
                        flag = False
                        break
                   
    return flag



def isConsistCR(PCM):
    flag = False
    N = len(PCM)
        
    if CONS_INDEX.ConsistencyRatio(PCM) <= CONS_INDEX.ConsistencyRatio_threshold(N):
        flag = True
    
    return flag



def isConsistGCI(PCM):
    flag = False
    N = len(PCM)

    if CONS_INDEX.GeometricConsistencyIndex(PCM) <= CONS_INDEX.GeometricConsistencyIndex_threshold(N):
        flag = True

    return flag



def isConsistHCR(PCM):
    flag = False
    N = len(PCM)

    if CONS_INDEX.HarmonicConsistencyRatio(PCM) <= CONS_INDEX.HarmonicConsistencyRatio_threshold(N):
        flag = True
        
    return flag
